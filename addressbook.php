<?php
include("header.php");
if (!(isset($_SESSION['pk']))){
header('location: login');
}else{

include("FUNCTIONS/fProfile.php");
include("FUNCTIONS/fGetAddressBook.php");

echo
'
<div class="container-fluid">
	<nav class="col-md-2">
		<ul class="nav nav-pills nav-stacked">
			<li>
				<span style="font-weight: 600; font-size: 18px" class="fa fa-user-circle-o"></span>
				<span style="font-weight: 600; font-size: 18px">'.$first_name.' '.$last_name.'</span>
			</li>
			<li><a href="personalinformation?u='.$_SESSION['username'].'" id="sideNavFont">• Personal Information <span class="fa fa-info"></span></a></li>
			<li><a href="wishlist?u='.$_SESSION['username'].'" id="sideNavFont">• Wishlist <span class="fa fa-magic"></span></a></li>
			<li><a href="reviews?u='.$_SESSION['username'].'" id="sideNavFont">• My Reviews <span class="fa fa-pencil"></span></a></li>
			<li class="active"><a href="addressbook?u='.$_SESSION['username'].'" id="sideNavFont">• Address Book <span class="fa fa-address-book"></span></a></li>
			<li><a href="notificationcenter?u='.$_SESSION['username'].'" id="sideNavFont">• Notification Center <span class="fa fa-exclamation-triangle"></span></a></li>
			<li><a href="buyandsell?u='.$_SESSION['username'].'" id="sideNavFont" >• Sell your preloved items <span class="fa fa-shopping-basket"></span></a></li>
			<li><a id="sideNavFont" href="../rewritten2/registration">• Be a seller! <span class="fa fa-briefcase"></span></a></li>
		</ul>
	</nav>
		<div class="col-md-10">
			<span style="font-size: 20px; font-weight: 600;">My Address Book</span>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12 text-center" style="margin-top: 15px">
						<span style="font-weight:600; font-size: 16px">New Address</span>
					</div>
					<div class="col-md-8 col-md-offset-2 text-center">';
					if (isset($_SESSION['success'])){
						echo $_SESSION['success'];
						}
					echo
					'	
					</div>
					<form action="FUNCTIONS/fAddAddressBook.php?n='.$_SESSION['pk'].'&e='.$_SESSION['email'].'" method="post">
						<div class="col-md-6">
							<div class="col-md-12" style="margin-top: 15px">
								<div class="col-md-12" style="background-color: #FFFFFF; margin-top:10px; padding: 20px; border:solid 1px black;">
									<div class="form-group">
										<div class="col-md-12 div-margin-bottom">
											<span class="address-label">Billing Address</span>
										</div>
										<div class="col-md-12">
											<span style="color:rgba(0, 0, 0, 0.6)">First and Last name *</span>
											<input type="text" class="form-control" name="txtBillingFullName" placeholder="Full Name" value="'.$first_name.' '.$last_name.'" required>
											<span style="color:rgba(0, 0, 0, 0.6)">Complete Address (House Number, Building and Street Name) *</span>
											<input type="text" class="form-control" name="txtBillingAddress" placeholder="Complete address" required>
											<span style="color:rgba(0, 0, 0, 0.6)">Province *</span>
											<input type="text" class="form-control" name="txtBillingProvince" placeholder="Province" required>
											<span style="color:rgba(0, 0, 0, 0.6)">Municipality *</span>
											<input type="text" class="form-control" name="txtBillingMunicipality" placeholder="Municipality" required>
											<span style="color:rgba(0, 0, 0, 0.6)">City/Baranggay *</span>
											<input type="text" class="form-control" name="txtBillingCity" placeholder="City" required>
											<span style="color:rgba(0, 0, 0, 0.6)">Phone Number *</span>
											<input type="text" class="form-control" name="txtBillingPhoneNumber" placeholder="Mobile/Phone Number" value="'.$mobile.'" required>
											<span style="color:rgba(0, 0, 0, 0.6)">Email *</span>
											<input type="email" class="form-control" name="txtBillingEmail" placeholder="Mobile/Phone Number" value="'.$email.'" required>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="col-md-12" style="margin-top: 15px">
								<div class="col-md-12" style="background-color: #FFFFFF; margin-top:10px; padding: 20px; border:solid 1px black;">
									<div class="form-group">
										<div class="col-md-12 div-margin-bottom">
											<span class="address-label">Shipping Address</span>
										</div>
										<div class="col-md-12">
											<span style="color:rgba(0, 0, 0, 0.6)">First and Last name *</span>
											<input type="text" class="form-control" name="txtShippingFullName" placeholder="Full Name" value="'.$first_name.' '.$last_name.'" required>
											<span style="color:rgba(0, 0, 0, 0.6)">Complete Address (House Number, Building and Street Name) *</span>
											<input type="text" class="form-control" name="txtShippingAddress" placeholder="Complete address" required>
											<span style="color:rgba(0, 0, 0, 0.6)">Province *</span>
											<input type="text" class="form-control" name="txtShippingProvince" placeholder="Province" required>
											<span style="color:rgba(0, 0, 0, 0.6)">Municipality *</span>
											<input type="text" class="form-control" name="txtShippingMunicipality" placeholder="Municipality" required>
											<span style="color:rgba(0, 0, 0, 0.6)">City/Baranggay *</span>
											<input type="text" class="form-control" name="txtShippingCity" placeholder="City" required>
											<span style="color:rgba(0, 0, 0, 0.6)">Phone Number *</span>
											<input type="text" class="form-control" name="txtShippingPhoneNumber" placeholder="Mobile/Phone Number" value="'.$mobile.'" required>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 text-center" style="padding: 10px">
						<button class="shrink" style="height: 50px; width: 200px; background-color: #6FBC92; border-radius; 0px; font-weight:600; color:white; margin-top: 10px" type="submit" name="save">Save</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-6" style="margin-top: 15px">
				<div class="col-md-12">
					<span style="font-weight:600; font-size: 16px">Billing Address</span>
				</div>';
				if ($billing_total > 0){
				while ($row = mysqli_fetch_array($getbilling)){
					echo
					'
				<form action="FUNCTIONS/fAddAddressBook.php?n='.$_SESSION['pk'].'&p='.$row['pk'].'" method="post">
					<div class="col-md-12">
						<div class="col-md-12" style="background-color: #FFFFFF; margin-top:10px; padding: 20px">
							<button class="close" aria-label="close" type="submit" name="removebilling">
								&times;
							</button>
							<div>
								<span>'.$row['full_name'].'</span>
							</div>
							<div>
								<span>'.$row['address'].', '.$row['city'].'</span>
							</div>
							<div>
								<span>'.$row['municipality'].', '.$row['province'].'</span>
							</div>
							<div>
								<span>'.$row['mobile'].'</span>
							</div>
							<div>
								<span>'.$row['email'].'</span>
							</div>
						</div>
					</div>
				</form> 

					';}}else{
						echo
						'
							<div class="col-md-12 text-center" style="margin-top: 10px"> 
								<span> You have no addresses. Create one now!</span>
							</div>
						';
					}
			echo
			'
			</div>
			<div class="col-md-6" style="margin-top: 15px">
				<div class="col-md-12">
					<span style="font-weight:600; font-size: 16px">Billing Address</span>
				</div>
				';
				if ($shipping_total > 0){
				while ($row = mysqli_fetch_array($getshipping)){
					echo
					'
				<form action="FUNCTIONS/fAddAddressBook.php?n='.$_SESSION['pk'].'&p='.$row['pk'].'" method="post">
					<div class="col-md-12">
						<div class="col-md-12" style="background-color: #FFFFFF; margin-top:10px; padding: 20px">
							<button class="close" aria-label="close" type="submit" name="removeshipping">
								&times;
							</button>
							<div>
								<span>'.$row['full_name'].'</span>
							</div>
							<div>
								<span>'.$row['address'].', '.$row['city'].'</span>
							</div>
							<div>
								<span>'.$row['municipality'].', '.$row['province'].'</span>
							</div>
							<div>
								<span>'.$row['mobile'].'</span>
							</div>
						</div>
					</div>
				</form> 

					';}}else{
						echo
						'
							<div class="col-md-12 text-center" style="margin-top: 10px"> 
								<span> You have no addresses. Create one now!</span>
							</div>
						';
					}
				echo
				'
				</div>
			</div>
		</div>
</div>
';
}
include('footer.php');