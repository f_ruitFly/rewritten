<?php
include('header.php');
echo
'
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-6 col-md-offset-3" style="background-color: #FFFFFF">
				<div class="text-center"><img src="ASSETS/image/chamee.png" style="width: 200px; height:auto"></div>
				<div class="text-center"><strong>Using ChamShop</strong></div><br>
				<p>
<li>post, list or upload content or items in inappropriate categories or areas on our sites;  will be deleted.<br><li>
You are responsible for reading the full item listing before making a bid or commitment to buy.<br><li>
use our Services if you are not able to form legally binding contracts (for example, if you are under 18 years old), or are temporarily or indefinitely suspended from using our sites,
fail to pay for items purchased by you, unless you have a valid reason as set out in an eBay policy, for example, the seller has materially changed the item\'s description after you bid, a clear typographical error is made, or you cannot contact the seller (contact us at champshopph@gmail.com)<br><li>
manipulate the price of any item or interfere with any other user\'s listings;<br>
post false, inaccurate, misleading, deceptive, defamatory, or libelous content;<br>
distribute or post spam, unsolicited or bulk electronic communications, chain letters, or pyramid schemes;<br><li>
distribute viruses or any other technologies that may harm ChamShop or the interests or property of users;<br><li>
use any robot, spider, scraper, data mining tools, data gathering and extraction tools, or other automated means to access our Services for any purpose, except with the prior express permission of ChamShop;<br><li>
interfere with the working of our Services, or impose an unreasonable or disproportionately large load on our infrastructure;<br><li>
Harvest or otherwise collect information about users without their consent.<br><li>
Sellers must meet ChamShop\'s minimum performance standards. Failure to meet these standards may result in ChamShop charging you additional fees, and/or limiting, restricting, suspending, or downgrading your seller account.<br><li>
If we believe you are abusing ChamShop in any way, we may, in our sole discretion and without limiting other remedies, limit, suspend, or terminate your user account(s) and access to our Services, delay or remove hosted content remove, not display, and/or demote listings, reduce or eliminate any discounts, and take technical and/or legal steps to prevent you from using our Services.<br><li>
We may cancel unconfirmed accounts or accounts that have been inactive for a long time or modify or discontinue our Services. <br><li>Additionally, we reserve the right to refuse or terminate all or part of our Services to anyone for any reason at our discretion.
<br>
<div class="text-center"><strong>Listing Conditions</strong></div>
When listing an item, you agree to comply with ChamShop’s rules for listing and Selling Practices policy.<br>
<li>You are responsible for the accuracy and content of the listing and item offered.<br><li>
Your listing may not be immediately searchable by keyword or category for several hours (or up to 24 hours in some circumstances). ChamShop can\'t guarantee exact listing durations.<br><li>
Content that violates any of ChamShop\'s policies may be modified, obfuscated or deleted at ChamShop\'s discretion.
				</p>
				</div>
			</div>
		</div>
	</div>
';
include('footer.php');