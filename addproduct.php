<?php
include('header.php');
if (!(isset($_SESSION['username']))){
	header('location:login');
}else{
	echo
	'
    <div class="container-fluid" style="margin-bottom: 20px;">
        <div id="addProducts" class="col-md-10 col-md-offset-1" style="background-color: #FFFFFF; padding: 20px">
            <h3 style="font-weight:600">Add Product</h3>
            <div class="row">
                <div class="col-md-12">                     
                <form autocomplete="off" action="FUNCTIONS/fAddProducts.php" method="post" enctype="multipart/form-data">
                    <div class="col-md-12">
                        <h3>Basic Information</h3>
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>Product Name</h5>
                        <input type="text" class="form-control" name="txtProductName" id="txtProductName" placeholder="Product Name" required size="1">
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>Product Description</h5>
                        <textarea class="form-control" rows="10" id="txtProductDescription" name="txtProductDescription" placeholder="Write your product description here. TIP: Clear and complete product details will definitely attract more customers"></textarea>
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>Product Category</h5>
                        <select class="form-control" name="cbMainProductCategory">
                            <option value="Mobiles, Phones, and Tablets">Mobiles, Phones, and Tablets</option>
                            <option value="Computers">Computers</option>
                            <option value="Consumer Electronics">Consumer Electronics</option>
                            <option value="Wholesale">Wholesale</option>
                            <option value="Home and Furniture">Home and Furniture</option>
                            <option value="Beauty, Health, and Grocery">Beauty, Health, and Grocery</option>
                            <option value="Clothing and Accessories">Clothing and Accessories</option>
                            <option value="Books, Sports, and Hobbies">Books, Sports, and Hobbies</option>
                            <option value="Baby Stuffs and Toys">Baby Stuffs and Toys</option>
                            <option value="Real Estate">Real Estate</option>
                            <option value="Cars and Automotives">Cars and Automotives</option>
                            <option value="Motorcycles and Scooters">Motorcycles and Scooters</option>
                            <option value="Services Jobs">Services Jobs</option>
                            <option value="Business and Earning Opportunities">Business and Earning Opportunities</option>
                            <option value="Construction and Farming">Construction and Farming</option>
                            <option value="Pets and Animals">Pets and Animals</option>
                            <option value="Heavy Machines and Trucks">Heavy Machines and Trucks</option>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <h3>Price</h3>
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>Price</h5>
                        <input type="text" class="form-control" name="txtPrice" id="txtPrice" placeholder="Price" required size="1">
                    </div>
                    <div class="col-md-12">
                        <h3>Location & Contact Number</h3>
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>Location</h5>
                        <input type="text" class="form-control" name="txtLocation" id="txtLocation" placeholder="Location" required size="1">
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>Contact Number</h5>
                        <input type="text" class="form-control" name="txtMobile" id="txtMobile" placeholder="Contact Number" required size="1">
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>Optional:</h5>
                        <input type="text" class="form-control" name="txtMobile2" id="txtMobile2" placeholder="Contact Number #2" size="1">
                        <input type="text" class="form-control" name="txtMobile3" id="txtMobile3" placeholder="Contact Number #3" size="1">
                        <input type="text" class="form-control" name="txtMobile4" id="txtMobile4" placeholder="Contact Number #4" size="1">
                    </div>
                    <div class="col-md-12">
                        <h3>Uploading of Image</h3>
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <input type="file" class="form-control" name="imgUpload[]" id="imgUpload" required size="1" accept="image/*" multiple>
                        <h5>You can upload multiple images by highlighting all the images you want to upload. </h5>
                    </div>
                    <div class="text-center" style="margin-top: 1% ">
                        <button style="width: 25%" type="submit" class="btn btn-success btn-md" name="addproduct"><span class="fa fa-check-circle"></span> Submit</button>
                        <button style="width: 25%" type="button" class="btn btn-danger btn-md"><span class="fa fa-minus-circle"></span> Cancel</button>
                    </div>
                </form>
                </div>
            </div> 
        </div>
    </div>
';
}
include('footer.php');