<?php
include("header.php");
include("FUNCTIONS/fGetSellerProducts.php");
echo
'
<div class="container-fluid">
 		<div class="row">
			<div class="col-md-12">
				<div style="margin-bottom: 10px">';
					if (isset($_SESSION['pk'])){
					if ($follow_added <= 0){
					echo
					'
					<form action="FUNCTIONS/fSellerStats.php?u='.$_SESSION['pk'].'&s='.$sellers_pk.'&b='.$_GET['u'].'" method="post">
						<button class="shrink" style="border:none; background-color:#6FBC92;font-weight:600; color:white;" type="submit" name="follow"> Follow</button>';
						if ($recommended_added <=0){
						echo
						'
						<button class="shrink" style="border:none; background-color:#6FBC92;font-weight:600; color:white;" type="submit" name="recommendshop">Recommend this shop</button>
						';
						}else{
						echo
						'
						<button class="shrink" style="border:none; background-color:#6FBC92;font-weight:600; color:white;" type="submit" name="removerecommendshop">Remove recommendation for this shop</button>
						';
						}
						echo
						'
					</form>
					';}else{
					echo
					'
					<form action="FUNCTIONS/fSellerStats.php?u='.$_SESSION['pk'].'&s='.$sellers_pk.'&b='.$_GET['u'].'" method="post">
						<button class="shrink" style="border:none; background-color:#6FBC92;font-weight:600; color:white;" type="submit" name="unfollow">Unfollow</button>';
					if ($recommended_added <=0){
						echo
						'
						<button class="shrink" style="border:none; background-color:#6FBC92;font-weight:600; color:white;" type="submit" name="recommendshop">Recommend this shop</button>
						';
						}else{
						echo
						'
						<button class="shrink" style="border:none; background-color:#6FBC92;font-weight:600; color:white;" type="submit" name="removerecommendshop">Remove recommendation for this shop</button>
						';
						}
						echo
						'
					</form>
					';}
				}
				echo
				'
				</div>
				<div class="col-md-12" style="background-color: #FFFFFF;">
					<div class="col-md-12" style="margin-top:20px;">
						<div class="col-md-1">
							<img class="img-rounded" style="width: 80px;" src="../uploads/'.$shop_image.'">
						</div>
						<div class="col-md-2 divider-vertical">
							<span style="font-weight:600">'.$store_name.'</span>
						</div>
						<div class="col-md-8 text-center">
							<div class="col-md-4 divider-vertical">
								<div>
									<span class="fa fa-tags"></span>
								</div>
								<div>
									<span>'.$product_count_result['result'].' products in shop.</span>
								</div>
							</div>
							<div class="col-md-4 divider-vertical">
								<div>
									<span class="fa fa-smile-o"></span>
								</div>
								<div>
									<span>'.$recommendation.' people recommended this shop.</span>
								</div>
							</div>
							<div class="col-md-4">
								<div>
									<span class="fa fa-users"></span>
								</div>
								<div>
									<span>'.$followers.' followers</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
 		</div>
	</div>
	<nav class="col-md-2" style="margin-top: 10px">
	<ul class="nav nav-pills nav-stacked">
		<div style="border: solid 1px black; background-color: #0D1D32">
			<li class="text-center"><a id="indexSideNav" data-toggle="pill" href="#accountSummary"> All </a></li>
		</div>
		<div style="border: solid 1px black; background-color:#FFFFFF" class="col-md-12">			
				<li class="text-center"><span style="color: rgba(0,0,0,0.6); font-size: 12px;">Main Product</span></li>
				<li style="margin-bottom:8px;"><a id="sellerSideNav">• '.$main_category.'</a></li>
				<li class="divider"></li>';
				while ($row = mysqli_fetch_array($getcategories)){
					echo
					'
					<li style="margin-bottom:8px;"><a id="sellerSideNav">• '.$row['category'].'</a></li>
					';
				}
			echo
			'
		</div>
	</ul>
</nav>

<div class="container-fluid" style="margin-top: 10px">
	<div class="row">
		<div class="col-md-10">
			<div class="col-md-12" style="background-color: #FFFFFF;">
			<div class="col-md-12">
				<div class="pull-right">
					<span style="font-size:12px;">Displaying '.$product_count_result['result'].' of '.$product_count_result['result'].' items</span>
				</div>
			</div>
			';
			while($row = mysqli_fetch_array($getproducts)){
			echo
			'
			<a style="text-decoration:none; color:black" href="item?p='.$row['product_name'].'&n='.$row['pk'].'&u='.$row['uploaded_by'].'">
				<div class="col-md-2 shrink" style="margin-bottom: 50px;">
					<div style="margin-top: 5px;" class="text-center">
						<img style="width: 120px; height: 120px" src="../uploads/'.$row['product_picture'].'">
					</div>
					<div class="text-center">
						<span>'.substr($row['product_name'],0,15).'</span>
					</div>
					<div>
						<span style="font-size: 12px; color: orange">₱ '.$row['price'].'</span>
						<span class="pull-right" style="color: rgba(0,0,0,.54); font-size: 12px;">Stock: '.$row['stock'].' </span>
					</div>
					<div>
						<span style="color: rgba(0,0,0,.54); font-size: 12px;"><span class="fa fa-heart-o"></span> '.$row['wishlist'].'</span>
						<span class="pull-right" style="color: rgba(0,0,0,.54); font-size: 12px;"><span class="fa fa-thumbs-up"></span> '.$row['recommended'].'</span>
					</div>
				</div>
			</a>
			';}
			echo
			'	
			</div>
		</div>
	</div>
</div>
';
