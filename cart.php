<?php
include('header.php');
include('FUNCTIONS/fCartContents.php');
if ($cart_total > 0){
echo
'
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-6">
					<div class="col-md-12">
					<div class="col-md-12" style="margin-bottom: 10px">
						<div>
							<span style="font-weight:600; font-size: 18px">My Cart Contents</span>
						</div>
						<div>
							<span>'.$cart_total.' products in cart</span>
						</div>
						<div class="col-md-12">';
						if (isset($_SESSION['success'])){
						echo $_SESSION['success'];
						}
						echo
						'
						</div>
					</div>
					';
						$total_price = 0;
						$total_items = 0;
					while ($row = mysqli_fetch_array($getcontents)){
						$product_pk = $row['product_pk'];
						$getproducts = mysqli_query($connect,"SELECT * FROM sellers_products WHERE pk = $product_pk");
						while ($products = mysqli_fetch_array($getproducts)){
						if ($products['sale'] == false){
						$total_price += $products['price'] * $row['amount_ordered'];
						}else{
						$total_price += $products['new_price'] * $row['amount_ordered'];
						}
						$total_items += $row['amount_ordered'];
						echo
						'					
						<div class="col-md-12" style="background-color: #FFFFFF; padding: 10px; margin-bottom: 10px">
						<a style="color: black" href="item?p='.$products['product_name'].'&n='.$products['pk'].'&u='.$products['uploaded_by'].'">
							<div class="col-md-2">
								<img style="width:70px; height:70px" src="../uploads/'.$products['product_picture'].'">
							</div>
						</a>
							<div class="col-md-2">
								<span>'.$products['product_name'].'</span>
								<div>
									<span style="color: rgba(242, 177, 0, 0.9); font-size:12px">Stocks: '.$products['stock'].'</span>
								</div>
							</div>
							<div class="col-md-2 col-md-offset-1 text-center">
								<div>
									<span style="color: rgba(0, 0, 0, 0.7);">Price</span>
								</div>
								<div>';
								if ($products['sale'] == false){
									echo
									'
									<span style="font-weight:600">₱ '.$products['price'].'</span>
									';
								}else{
									echo 
									'
									<span style="font-weight:600">₱ '.$products['new_price'].'</span>
									';
								}
								echo
								'
								</div>
							</div>
							<div class="col-md-2 text-center">
								<div>
									<span>Quantity</span>
								</div>
								<div>
									<span style="font-weight:600">'.$row['amount_ordered'].'</span>
								</div>
							</div>
							<div class="col-md-2 pull-right">
								<form action="FUNCTIONS/fCart.php?p='.$row['pk'].'" method="post">
									<button class="close" aria-label="close" type="submit" name="remove">
										&times;
									</button>
								</form>
							</div>
						</div>
						';
					}}
					echo
					'
					</div>
				</div>
				<div class="col-md-4 col-md-offset-1">
					<div class="col-md-12" style="margin-bottom: 10px">
						<div>
							<span style="font-weight:600; font-size: 18px">Summary</span>
						</div>
					</div>
					<div class="col-md-12 text-center" style="background-color: #FFFFFF;">
						<div class="col-md-12" style="margin-bottom: 5px">
							<div class="col-md-6">
								<span>Number of Items:</span>
							</div>
							<div class="col-md-6">
								<span>'.$total_items.'</span>
							</div>
						</div>
						<div class="col-md-12" style="margin-bottom: 5px">
							<div class="col-md-6">
								<span>Subtotal:</span>
							</div>
							<div class="col-md-6">
								<span>₱ '.$total_price.'</span>
							</div>
						</div>
						<div class="col-md-12" style="margin-bottom: 5px">
							<div class="col-md-12" style="border: 1px solid #000000; margin-bottom: 5px" ></div>
							<div class="col-md-6">
								<span>Total:</span>
							</div>
							<div class="col-md-6">
								<span>₱ '.$total_price.'</span>
							</div>
						</div>
					</div>
					<a href="checkout?u='.$_COOKIE['unique'].'">
						<button class="col-md-12 shrink" style="height: 50px; background-color: #6FBC92; border-radius; 0px; font-weight:600; color:white; margin-top: 10px">Proceed to Checkout
						</button>
					</a>
				</div>
			</div>
		</div>
	</div>
';
}else{
echo
'
<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 text-center">
				<span>You have no items in your cart, please add one.</span>
			</div>
		</div>
	</div>
';
}