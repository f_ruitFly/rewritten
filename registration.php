<?php
include ("header.php");

echo
'
<script src="JS/form-validation.js"></script>
<div class="container" style="margin-bottom: 10px">
	<div class="row">
		<div style="border: solid 1px #AAAAAA; background-color: #dddddd;" class="col-md-8 col-md-offset-2">
			<div class="text-center">
				<img class="img-rounded" style="width: 180px; height:auto" src="ASSETS/IMAGE/chamee.png">
			</div>
			<div class="col-md-12 text-center">
				<h2 class="modal-title">Registration</h2>
			</div>
			<div class="col-md-12" style="margin-top:2%">
				<h3 class="modal-title">Welcome Shopper!</h3>
			</div>
			<div class="col-md-12">
				<h5 class="modal-title">I`m <span style="font-weight:900">Huni!</span> Your friendly chameleon.</h5>
			</div>
			<div class="col-md-12">
				<h5 class="modal-title">Please fill up all the fields in order to maximize your shopping experience. Enjoy browsing and shopping!</h5>
			</div>
			<div class="col-md-12">
				<form id="form-registration" autocomplete="off" style="margin-top:2%" action="FUNCTIONS/fRegistration.php" method="post">
					<span class="fa fa-user-circle-o"></span> <label>Full Name *</label>
					<div class="form-group">
						<input type="text" class="form-control" name="txtFirstName" id="txtFirstName" placeholder="Enter First Name" required size="1">
						<input type="text" class="form-control" name="txtLastName" id="txtLastName" placeholder="Enter Last Name" required size="1">		
					</div>
					<div class="form-group">
					<span class="fa fa-envelope"></span> <label for="txtEmail">E-mail *</label>
						<input type="email" class="form-control" name="txtEmail" id="txtEmail" placeholder="example@email.com" required size="1">
						<span id="status_email"></span>
					</div>
					<div class="form-group">
					<span class="fa fa-mobile" style="font-size:20px"></span> <label for="txtMobile">Mobile Number *</label>
						<input type="text" class="form-control" name="txtMobile" id="txtMobile" placeholder="Enter your mobile number" required size="1">
						<span id="status_mobile"></span>
					</div>
					<div class="form-group">
					<span class="fa fa-user-secret"></span> <label>Username & Password *</label>
						<input type="text" class="form-control" name="txtUsername" id="txtUsername" placeholder="Enter Username" required size="43">
						<span id="status_username"></span>
						<input type="password" class="form-control" name="txtPassword" id="txtPassword" placeholder="Enter Password" required size="43">
						<span id="status_password"></span>
					</div>
					<div style="margin-top: 3%">
						<span>By clicking submit, means you have agreed to <a style="font-weight:600; text-decoration:none">terms and conditions</a> of using ChamShop.</span>
					</div>
					<div style="margin-top: 1%">
						<button style="width: 25%" type="submit" class="btn btn-success btn-md" name="register" id="register"><span class="fa fa-check-circle"></span> Submit</button>
						<a href="../rewritten"><button style="width: 25%" type="button" class="btn btn-danger btn-md"><span class="fa fa-minus-circle"></span> Cancel</button></a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
';
include('footer.php');