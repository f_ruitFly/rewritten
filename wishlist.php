<?php
include("header.php");
if (!(isset($_SESSION['pk']))){
header('location: login');
}else{

include ("FUNCTIONS/fGetWishlist.php");

echo
'
<div class="container-fluid">
	<nav class="col-md-2">
		<ul class="nav nav-pills nav-stacked">
			<li>
				<span style="font-weight: 600; font-size: 18px" class="fa fa-user-circle-o"></span>
				<span style="font-weight: 600; font-size: 18px">'.$first_name.' '.$last_name.'</span>
			</li>
			<li><a href="personalinformation?u='.$_SESSION['username'].'" id="sideNavFont">• Personal Information <span class="fa fa-info"></span></a></li>
			<li class="active"><a href="wishlist?u='.$_SESSION['username'].'" id="sideNavFont">• Wishlist <span class="fa fa-magic"></span></a></li>
			<li><a href="reviews?u='.$_SESSION['username'].'" id="sideNavFont">• My Reviews <span class="fa fa-pencil"></span></a></li>
			<li><a href="addressbook?u='.$_SESSION['username'].'" id="sideNavFont">• Address Book <span class="fa fa-address-book"></span></a></li>
			<li><a href="notificationcenter?u='.$_SESSION['username'].'" id="sideNavFont">• Notification Center <span class="fa fa-exclamation-triangle"></span></a></li>
			<li><a id="sideNavFont" href="../rewritten2/registration">• Be a seller! <span class="fa fa-briefcase"></span></a></li>
		</ul>
	</nav>
	<div id="myWishlist">
		<div class="col-md-10">
			<span style="font-size: 20px; font-weight: 600;">My Wishlist</span>
				<div class="row">
					<div class="col-md-12">';
					while ($wish = mysqli_fetch_array($getwishlist)){
						$product_pk = $wish['product_pk'];
						$loopwishlist = mysqli_query($connect, "SELECT * FROM sellers_products WHERE pk = $product_pk");
						while ($row = mysqli_fetch_array($loopwishlist)){
						echo
						'
						<a style="text-decoration:none; color:black;" href="item?p='.$row['product_name'].'&n='.$row['pk'].'&u='.$row['uploaded_by'].'">
							<div class="col-md-12" style="padding:10px; background-color: #FFFFFF;">
								<div class="col-md-2">
									<img style="width: 150px; height: 150px" href="../uploads/'.$row['product_picture'].'">
								</div>
								<div class="col-md-10">
									<span style="font-size: 18px; font-weight: 600">'.$row['product_name'].'</span>
								</div>
								<div class="col-md-10">
									<span style="font-size: 16px; color: orange">₱'.$row['price'].'</span>
								</div>
								<div class="col-md-10">
									<span style="font-size: 14px; color: rgba(0,0,0,.54);">'.$row['product_description'].'</span>
								</div>
							</div>
						</a>
						';}}
					echo
					'
					</div>
				</div>
		</div>
	</div>
</div>
';
}

include('footer.php');