<?php
$connect = mysqli_connect('localhost', 'root', '', 'huni');
include_once('fLogin.php');
if (isset($_POST['save'])){
	$pk = $_GET['n'];
	$email = $_GET['e'];
	$txtBillingFullName = mysqli_real_escape_string($connect, $_POST['txtBillingFullName']);
	$txtBillingAddress = mysqli_real_escape_string($connect, $_POST['txtBillingAddress']);
	$txtBillingProvince = mysqli_real_escape_string($connect, $_POST['txtBillingProvince']);
	$txtBillingMunicipality = mysqli_real_escape_string($connect, $_POST['txtBillingMunicipality']);
	$txtBillingCity = mysqli_real_escape_string($connect, $_POST['txtBillingCity']);
	$txtBillingPhoneNumber = mysqli_real_escape_string($connect, $_POST['txtBillingPhoneNumber']);
	$txtBillingEmail = mysqli_real_escape_string($connect, $_POST['txtBillingEmail']);
	$txtShippingFullName = mysqli_real_escape_string($connect, $_POST['txtShippingFullName']);
	$txtShippingAddress = mysqli_real_escape_string($connect, $_POST['txtShippingAddress']);
	$txtShippingProvince = mysqli_real_escape_string($connect, $_POST['txtShippingProvince']);
	$txtShippingMunicipality = mysqli_real_escape_string($connect, $_POST['txtShippingMunicipality']);
	$txtShippingCity = mysqli_real_escape_string($connect, $_POST['txtShippingCity']);
	$txtShippingPhoneNumber = mysqli_real_escape_string($connect, $_POST['txtShippingPhoneNumber']);

	$query = "INSERT INTO billing_address (full_name, address, province, municipality, city, mobile, email, account_pk, archived) VALUES ('$txtBillingFullName', '$txtBillingAddress', '$txtBillingProvince', '$txtBillingMunicipality', '$txtBillingCity', '$txtBillingPhoneNumber', '$txtBillingEmail', $pk, false)";
	$query2 = "INSERT INTO shipping_address (full_name, address, province, municipality, city, mobile, account_pk, archived) VALUES ('$txtShippingFullName', '$txtShippingAddress', '$txtShippingProvince', '$txtShippingMunicipality', '$txtShippingCity', '$txtShippingPhoneNumber', $pk, false)"; 
	mysqli_query($connect, $query);
	mysqli_query($connect, $query2);
	$_SESSION['success'] =
	'
		<form action="FUNCTIONS/fAddAddresBook.php" method="post">
		<div class="alert alert-success alert-dismissable text-center">
		  <button class="close" aria-label="close" type="submit" name="closealert">&times;</button>
		  <span>Successfully added to the addressbook.</span>
		</div>
		</form>
	';
	header('location:'.$_SERVER['HTTP_REFERER']);
}

if (isset($_POST['removeshipping'])){
	$account_pk = $_GET['n'];
	$pk = $_GET['p'];
	$query = "UPDATE shipping_address SET archived = true WHERE account_pk = $account_pk AND pk = $pk";
	mysqli_query($connect, $query);
	header('location:'.$_SERVER['HTTP_REFERER']);
}
if (isset($_POST['removebilling'])){
	$account_pk = $_GET['n'];
	$pk = $_GET['p'];
	$query = "UPDATE billing_address SET archived = true WHERE account_pk = $account_pk AND pk = $pk";
	mysqli_query($connect, $query);
	header('location:'.$_SERVER['HTTP_REFERER']);
}

if (isset($_POST['closealert'])){
	unset($_SESSION['success']);
	header('location:../addressbook?u='.$_SESSION['username']);
}