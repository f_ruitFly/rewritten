<?php
include("fGetSessions.php");
include("fGetItemProducts.php");
$connect = mysqli_connect('localhost', 'root', '', 'huni');

$product_pk = $_GET['n'];

// ADD COMMENT
if (isset($_POST['addcomment'])){
	$txtComment = mysqli_real_escape_string($connect, $_POST['txtComment']);
	$username = $_SESSION['username'];
	$uploaded_by = $_GET['u'];

	$query = "INSERT INTO product_comments (commenter_username, product_pk, comment, date_commented, time_commented) VALUES ('$username', $product_pk, '$txtComment', curdate(), curtime())";
	mysqli_query($connect, $query);
	$query2 = "INSERT INTO notifications (username, uploaded_by, product_pk, message, date_notified, time_notified, archived) VALUES ('$username', '$uploaded_by', $product_pk, 'commented in your product.', curdate(), curtime(), false)";
	mysqli_query($connect, $query2);
	header('location:'.$_SERVER['HTTP_REFERER']);
}

// RECOMMEND ITEM
if (isset($_POST['recommend'])){
	$username = $_SESSION['username'];
	$uploaded_by = $_GET['u'];
	$recommended_total = ($recommended+1);
	$query = "INSERT INTO product_recommendation (username, product_pk, date_recommended, time_recommended) VALUES ('$username', $product_pk, curdate(), curtime())";
	$query2 ="UPDATE sellers_products SET recommended = $recommended_total WHERE pk = $product_pk";
	$query3 = "INSERT INTO notifications (username, uploaded_by, product_pk, message, date_notified, time_notified, archived) VALUES ('$username', '$uploaded_by', $product_pk, 'recommended your product.', curdate(), curtime(), false)";
	mysqli_query($connect, $query);
	mysqli_query($connect, $query2);
	mysqli_query($connect, $query3);
	header('location:'.$_SERVER['HTTP_REFERER']);
}

if (isset($_POST['donotrecommend'])){
	$username = $_SESSION['username'];
	$recommended_total = ($recommended-1);
	$query = "UPDATE product_recommendation SET archived = true WHERE username = '$username'";
	$query2 ="UPDATE sellers_products SET recommended = $recommended_total WHERE pk = $product_pk";
	mysqli_query($connect, $query);
	mysqli_query($connect, $query2);
	header('location:'.$_SERVER['HTTP_REFERER']);
}


// WISHLIST ITEM
if (isset($_POST['addtowishlist'])){
	$username = $_SESSION['username'];
	$wishlist_total = ($wishlist+1);
	$query = "INSERT INTO user_wishlist (username, product_pk, date_listed, time_listed) VALUES ('$username', $product_pk, curdate(), curtime())";
	$query2 ="UPDATE sellers_products SET wishlist = $wishlist_total WHERE pk = $product_pk";
	$query3 = "INSERT INTO notifications (username, uploaded_by, product_pk, message, date_notified, time_notified, archived) VALUES ('$username', '$uploaded_by', $product_pk, 'added your product in their wishlist.', curdate(), curtime(), false)";
	mysqli_query($connect, $query);
	mysqli_query($connect, $query2);
	mysqli_query($connect, $query3);
	header('location:'.$_SERVER['HTTP_REFERER']);
}

if (isset($_POST['removetowishlist'])){
	$username = $_SESSION['username'];
	$wishlist_total = ($wishlist-1);
	$query = "UPDATE user_wishlist SET archived = true WHERE username = '$username'";
	$query2 ="UPDATE sellers_products SET wishlist = $wishlist_total WHERE pk = $product_pk";
	mysqli_query($connect, $query);
	mysqli_query($connect, $query2);
	header('location:'.$_SERVER['HTTP_REFERER']);
}