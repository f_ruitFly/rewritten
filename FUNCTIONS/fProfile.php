<?php

$connect = mysqli_connect('localhost', 'root', '', 'huni');
include_once('fLogin.php');
// GET USER INFORMATION
$username = $_SESSION['username'];
$userInformation = mysqli_query($connect, "SELECT * FROM users WHERE username ='$username'");

while ($row = mysqli_fetch_array($userInformation)) {
	$first_name = $row['first_name'];
	$last_name = $row['last_name'];
	$username = $row['username'];
	$email = $row['email'];
	$mobile = $row['mobile'];
}

//PROFILE - PERSONAL INFORMATION - *UPDATE MODAL
if (isset($_POST['saveUpdate'])){
	$txtFirstName = mysqli_real_escape_string($connect, $_POST['txtFirstName']);
	$txtLastName = mysqli_real_escape_string($connect, $_POST['txtLastName']);
	$txtMobile = mysqli_real_escape_string($connect, $_POST['txtMobile']);

	$query = "UPDATE users SET first_name = '$txtFirstName', last_name = '$txtLastName', address = '$txtAddress', mobile = '$txtMobile' WHERE username='$username'";

	mysqli_query($connect, $query);
	header('location:'.$_SERVER['HTTP_REFERER']); 
}

//PROFILE - PERSONAL INFORMATION - *UPDATE MODAL = E-MAIL
if (isset($_POST['saveEmail'])){
	$txtEmail = mysqli_real_escape_string($connect, $_POST['txtEmail']);

	$query = "UPDATE users SET email = '$txtEmail' WHERE username='$username'";

	mysqli_query($connect, $query);
	header('location:'.$_SERVER['HTTP_REFERER']); 
}

//PROFILE - PERSONAL INFORMATION - *UPDATE MODAL = PASSWORD
if (isset($_POST['savePassword'])){
	$txtNewPassword = mysqli_real_escape_string($connect, $_POST['txtNewPassword']);
	$txtRetypePassword = mysqli_real_escape_string($connect, $_POST['txtRetypePassword']);

	$hashedPassword = password_hash($txtNewPassword, PASSWORD_DEFAULT);

	$query = "UPDATE users SET password = '$hashedPassword' WHERE username='$username'";

	mysqli_query($connect, $query);
	header('location:'.$_SERVER['HTTP_REFERER']); 
}

//PROFILE - GET WISHLIST + COUNT
$getwishlist_count= mysqli_query($connect, "SELECT COUNT(*) AS result FROM user_wishlist WHERE  username = '$username' AND archived = false");
$getwishlist_count_result = mysqli_fetch_array($getwishlist_count);
$wishlist = $getwishlist_count_result['result'];

//PROFILE - GET COMMENTS/REVIEWED
$getcomment_count= mysqli_query($connect, "SELECT COUNT(*) AS result FROM product_comments WHERE  commenter_username = '$username' AND archived = false");
$getcomment_count_result = mysqli_fetch_array($getcomment_count);
$comments = $getcomment_count_result['result'];

//PROFILE - GET RECOMMENDATION
$getrecommendation_count = mysqli_query($connect, "SELECT COUNT(*) AS result FROM product_recommendation WHERE  username = '$username' AND archived = false");
$getrecommendation_count_result = mysqli_fetch_array($getrecommendation_count);
$recommendation = $getrecommendation_count_result['result'];

