<?php
$connect = mysqli_connect('localhost', 'root', '', 'huni');
include('fLogin.php');

if (isset($_POST['addproduct'])){
	$username = $_SESSION['username'];
	$txtProductName = mysqli_real_escape_string($connect, $_POST['txtProductName']);
	$txtProductDescription = mysqli_real_escape_string($connect, $_POST['txtProductDescription']);
	$cbMainProductCategory = mysqli_real_escape_string($connect, $_POST['cbMainProductCategory']);
	$txtPrice = mysqli_real_escape_string($connect, $_POST['txtPrice']);
	$txtLocation = mysqli_real_escape_string($connect, $_POST['txtLocation']);
    $txtMobile = mysqli_real_escape_string($connect, $_POST['txtMobile']);
    $txtMobile2 = mysqli_real_escape_string($connect, $_POST['txtMobile2']);
    $txtMobile3 = mysqli_real_escape_string($connect, $_POST['txtMobile3']);
    $txtMobile4 = mysqli_real_escape_string($connect, $_POST['txtMobile4']); 
    
    $total = count($_FILES['imgUpload']['name']);

    // PRODUCT NAME AND OTHER INFORMATION
	    $query = "INSERT INTO user_products (product_name, product_description, product_category, uploaded_by, price, location, mobile, mobile2, mobile3, mobile4) VALUES ('$txtProductName', '$txtProductDescription', '$cbMainProductCategory', '$username', $txtPrice, '$txtLocation', '$txtMobile', '$txtMobile2', '$txtMobile3', '$txtMobile4')";
	    mysqli_query($connect, $query);

    //PRODUCT PICTURE 
	for ($i=0; $i<$total; $i++){  
		$newFileName = randomPrefix(50).".".end(explode(".", $_FILES["imgUpload"]["name"][$i]));
	    $folder='/xampp/htdocs/uploads/'; 
		move_uploaded_file($_FILES["imgUpload"]["tmp_name"][$i], "$folder"."$newFileName");
		$picture_query = "INSERT INTO user_product_pictures (product_name, product_picture, uploaded_by, archived) VALUES ('$txtProductName', '$newFileName', '$username', false)";
		mysqli_query($connect, $picture_query);
	}
	$updatepicture = "UPDATE user_products SET 	product_picture = '$newFileName' WHERE uploaded_by = '$username' AND product_name = '$txtProductName'";
	mysqli_query($connect,$updatepicture);
	$_SESSION['success'] = 
					'
					<form action="FUNCTIONS/fAddProducts.php" method="post">
					<div class="alert alert-success alert-dismissable text-center">
					  <button class="close" aria-label="close" type="submit" name="closealert">&times;</button>
					  <span>Item successfully added!</span>
					</div>
					</form>
					';
	header('location:../buyandsell?u='.$_SESSION['username']);
}

if (isset($_POST['manageupload'])){
	$username = $_SESSION['username'];
	$product_name = $_GET['n'];
    
   $total = count($_FILES['imgUpload']['name']);

    //PRODUCT PICTURE 
	for ($i=0; $i<$total; $i++){  
		$newFileName = randomPrefix(50).".".end(explode(".", $_FILES["imgUpload"]["name"][$i]));
	    $folder='/xampp/htdocs/uploads/'; 
		move_uploaded_file($_FILES["imgUpload"]["tmp_name"][$i], "$folder"."$newFileName");
		$picture_query = "INSERT INTO user_product_pictures (product_name, product_picture, uploaded_by, archived) VALUES ('$product_name', '$newFileName', '$username', false)";
		mysqli_query($connect, $picture_query);
		header('location:'.$_SERVER['HTTP_REFERER']);
	}	
}

if (isset($_POST['closealert'])){
	unset($_SESSION['success']);
	header('location:../buyandsell?u='.$_SESSION['username']);
}
function randomPrefix($length){
$random= '';
srand((double)microtime()*1000000);

$data = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

for($i = 0; $i < $length; $i++){
	$random .= substr($data, (rand()%(strlen($data))), 1);
}

return $random;
}