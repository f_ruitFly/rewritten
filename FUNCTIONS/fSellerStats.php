<?php
include("fGetSessions.php");
include("fGetSellerProducts.php");
$connect = mysqli_connect('localhost', 'root', '', 'huni');

if(isset($_POST['follow'])){
	$username = $_SESSION['username'];
	$sellers_pk = $_GET['s'];
	$product_pk = $_GET['n'];
	$uploaded_by = $_GET['b'];
	$query = "INSERT INTO sellers_followers (username, sellers_pk, date_followed, time_followed, archived) VALUES ('$username', $sellers_pk, curdate(), curtime(), false)";
	$query2 = "INSERT INTO notifications (username, uploaded_by, message, date_notified, time_notified, archived) VALUES ('$username', '$uploaded_by', 'is now following your shop.', curdate(), curtime(), false)";
	mysqli_query($connect, $query);
	mysqli_query($connect, $query2);
	header('location:'.$_SERVER['HTTP_REFERER']);
}

if(isset($_POST['unfollow'])){
	$username = $_SESSION['username'];
	$sellers_pk = $_GET['s'];
	$query = "UPDATE sellers_followers SET archived = true WHERE username = '$username' AND sellers_pk = $sellers_pk";
	mysqli_query($connect, $query);
	header('location:'.$_SERVER['HTTP_REFERER']);
}

if(isset($_POST['recommendshop'])){
	$username = $_SESSION['username'];
	$sellers_pk = $_GET['s'];
	$product_pk = $_GET['n'];
	$uploaded_by = $_GET['b'];
	$query = "INSERT INTO sellers_recommendation (username, sellers_pk, date_recommended, time_recommended, archived) VALUES ('$username', $sellers_pk, curdate(), curtime(), false)";
	$query2 = "INSERT INTO notifications (username, uploaded_by, message, date_notified, time_notified, archived) VALUES ('$username', '$uploaded_by', 'recommended your shop.', curdate(), curtime(), false)";
	mysqli_query($connect, $query);
	mysqli_query($connect, $query2);
	header('location:'.$_SERVER['HTTP_REFERER']);
}

if(isset($_POST['removerecommendshop'])){
	$username = $_SESSION['username'];
	$sellers_pk = $_GET['s'];
	$query = "UPDATE sellers_recommendation SET archived = true WHERE username = '$username' AND sellers_pk = $sellers_pk";
	mysqli_query($connect, $query);
	header('location:'.$_SERVER['HTTP_REFERER']);
}