<?php
include('header.php');
if (($_GET['category'] == null)){
	header('location:search?category=all&date=newest-to-oldest&price=newest-to-oldest');
}else{
include('FUNCTIONS/fGetIndexProducts.php');
echo
'
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">				
				<nav class="col-md-2" style="margin-bottom:10px">
					<ul class="nav nav-pills nav-stacked">
						<div style="border: solid 1px black; background-color: #0D1D32">
							<li class="text-center"><a id="indexSideNav" data-toggle="pill" href="#accountSummary"> All </a></li>
						</div>
						<div style="border: solid 1px black; background-color:#FFFFFF" class="col-md-12">		
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Bags-and-Travel&date=newest-to-oldest&price=lowest-to-highest">• Bags and Travel </a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Bedding-and-Bath&date=newest-to-oldest&price=lowest-to-highest">• Bedding and Bath</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Cameras&date=newest-to-oldest&price=lowest-to-highest">• Cameras</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Computers-and-Laptops&date=newest-to-oldest&price=lowest-to-highest">• Computers and Laptops</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Digital-Goods&date=newest-to-oldest&price=lowest-to-highest">• Digital Goods</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Fashion&date=newest-to-oldest&price=lowest-to-highest">• Fashion</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Furniture-and-Decor&date=newest-to-oldest&price=lowest-to-highest">• Furniture and Decor</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Groceries&date=newest-to-oldest&price=lowest-to-highest">• Groceries</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Health-and-Beauty&date=newest-to-oldest&price=lowest-to-highest">• Health and Beauty</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Home-Appliances&date=newest-to-oldest&price=lowest-to-highest">• Home Appliances</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Kitchen-and-Dining&date=newest-to-oldest&price=lowest-to-highest">• Kitchen and Dining</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Laundry-and-Cleaning&date=newest-to-oldest&price=lowest-to-highest">• Laundry and Cleaning</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Media-Music-and-Books&date=newest-to-oldest&price=lowest-to-highest">• Media, Music and Books</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Mobiles-and-Tablets&date=newest-to-oldest&price=lowest-to-highest">• Mobiles and Tablets</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Mother-and-Baby&date=newest-to-oldest&price=lowest-to-highest">• Mother and Baby</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Motors&date=newest-to-oldest&price=lowest-to-highest">• Motors</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Pet-Supplies&date=newest-to-oldest&price=lowest-to-highest">• Pet Supplies</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Sports-and-Outdoors&date=newest-to-oldest&price=lowest-to-highest">• Sports and Outdoor</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Stationary-and-Craft&date=newest-to-oldest&price=lowest-to-highest">• Stationary and Craft</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=TV-Audio-Gaming-and-Wearables&date=newest-to-oldest&price=lowest-to-highest">• TV, Audio, Gaming and Wearables</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Tools-DIY-and-Outdoor&date=newest-to-oldest&price=lowest-to-highest">• Tools, DIY and Outdoor</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Toys-and-Games&date=newest-to-oldest&price=lowest-to-highest">• Toys and Games</a></li>
								<li style="margin-bottom:8px;"><a id="sellerSideNav" href="search?category=Watches-Sun-Glasses-and-Jewellery&date=newest-to-oldest&price=lowest-to-highest">• Watches, Sun Glasses and Jewellery</a></li>
						</div>
					</ul>
				</nav>
				<div class="col-md-10">
					<div class="col-md-12" style="border: solid 1px black; background-color: #0D1D32">
						<form action="search?">
							<input type="hidden" value="'.$_GET['category'].'" name="category">
							<label style="color: #FFFFFF">Date:</label>
							<select name="date">
								<option value="newest-to-oldest">Newest to Oldest</option>
								<option value="oldest-to-newest">Oldest to Newest</option>
							</select>
							<label style="color: #FFFFFF">Price:</label>
							<select name="price">
								<option value="lowest-to-highest">Lowest to Highest</option>
								<option value="highest-to-lowest">Highest to Lowest</option>
							</select>
							<button type="submit">Go</button>
						</form>
					</div>
					<div class="col-md-12" style="border: solid 1px black; background-color: #FFFFFF">';
						while ($row = mysqli_fetch_array($getproducts)){
							echo
							'
							<a style="text-decoration:none; color:black" href="item?p='.$row['product_name'].'&n='.$row['pk'].'&u='.$row['uploaded_by'].'">
							<div class="col-md-2 shrink">
								<div style="margin-top: 5px;" class="text-center">
									<img style="width: 120px; height: 120px" src="../uploads/'.$row['product_picture'].'">
								</div>
								<div class="text-center">
									<span>'.substr($row['product_name'],0,15).'</span>
								</div>
								<div>';
								if ($row['sale'] == false){
									echo
									'
									<span style="font-size: 12px; color: orange">₱ '.$row['price'].'</span>
									';
								}else{
									echo
									'
									<span style="font-size: 12px; color: orange"><del>₱ '.$row['price'].'</del> ₱ '.$row['new_price'].'</span>
									';
								}
								echo
								'	
									<span class="pull-right" style="color: rgba(0,0,0,.54); font-size: 12px;">Stock: '.$row['stock'].' </span>
								</div>
								<div>
									<span style="color: rgba(0,0,0,.54); font-size: 12px;"><span class="fa fa-heart-o"></span> '.$row['wishlist'].'</span>
									<span class="pull-right" style="color: rgba(0,0,0,.54); font-size: 12px;"><span class="fa fa-thumbs-up"></span> '.$row['recommended'].'</span>
								</div>
							</div>
							</a>
							';}
							echo
							'
					</div>
				</div>				
			</div>
		</div>
	</div>
';
}
include('footer.php');

/**
<form action="search?">
		<input type="hidden" value="'.$_GET['category'].'" name="category">
		<select name="date">
			<option value="oldest-to-newest">Oldest to Newest</option>
		</select>
		<select name="price">
			<option value="highest-to-lowest">Highest to Lowest</option>
		</select>
		<button type="submit">Go</button>
	</form>
**/