<?php
include('header.php');
if (!(isset($_SESSION['username']))){
	header('location:login');
}else{
include("FUNCTIONS/fManageProducts.php");
	echo
	'
    <div class="container-fluid">
        <div id="addProducts" class="col-md-10 col-md-offset-1" style="background-color: #FFFFFF">
            <h3 style="font-weight:600"></h3>
            <div class="row">
                <div class="col-md-12">                     
                 	<div class="col-md-12">
                        <h3>Images Uploaded</h3>
                    </div>
                    <div class="col-md-12">';
                    while ($row = mysqli_fetch_array($getimages)){
                    echo
                    '
                    <form action="FUNCTIONS/fManageProducts?p='.$row['pk'].'&u='.$_SESSION['username'].'" method="post">
                    		<div class="col-md-2 text-center" style="margin-bottom: 1%; margin-top:1%">
                                    <a data-toggle="modal" data-target="#showPicturesModal"><img style="width:100px; height:100px; border-top: 1px solid #172438; border-bottom: 1px solid #172438; padding: 2px;" src="../uploads/'.$row["product_picture"].'"></a>
                                    <div class="text-center" style="margin-bottom: 3%; margin-top:3%">
                                         <button class="btn btn-default text-center" type="submit" name="remove">Remove</button>
                                    </div>
                            </div>
                    </form>
                    ';
                    }
   					echo
   					'
                    </div>
                <form action="FUNCTIONS/fAddProducts.php?n='.$_GET['n'].'&u='.$_SESSION['username'].'" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-8 col-md-offset-2">
                    	<div class="text-center">
                    		<h3>Add Images</h3>
                    	</div>
                        <input type="file" class="form-control" name="imgUpload[]" id="imgUpload" size="1" accept="image/*" required multiple>
                        <h5>You can upload multiple images by highlighting all the images you want to upload. </h5>
                        <div class="text-center" style="margin-bottom:10px; margin-top:10px">
                    			<button class="btn btn-warning" type="submit" name="manageupload">Upload</button>
                		</div>
                    </div>
                </form>
                <form autocomplete="off" action="FUNCTIONS/fManageProducts.php?u='.$_SESSION['username'].'&p='.$pk.'" method="post">
                    <div class="col-md-12">
                        <h3>Basic Information</h3>
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>Product Name</h5>
                        <a data-toggle="modal" data-target="#showPicturesModal">
                            <input type="text" class="form-control" name="txtProductName" id="txtProductName" placeholder="Product Name" required size="1" value="'.$product_name.'">
                        </a>
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>Product Description</h5>
                        <textarea class="form-control" rows="10" id="txtProductDescription" name="txtProductDescription" placeholder="Write your product description here. TIP: Clear and complete product details will definitely attract more customers">'.$product_description.'
                        </textarea>
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>Product Category</h5>
                        <h5>Current selected category: <span style="font-weight:600;">'.$product_category.'</span></h5>
                        <select class="form-control" name="cbMainProductCategory">
                            <option value="'.$product_category.'">'.$product_category.'</option>
                            <option value="Mobiles, Phones, and Tablets">Mobiles, Phones, and Tablets</option>
                            <option value="Computers">Computers</option>
                            <option value="Consumer Electronics">Consumer Electronics</option>
                            <option value="Wholesale">Wholesale</option>
                            <option value="Home and Furniture">Home and Furniture</option>
                            <option value="Beauty, Health, and Grocery">Beauty, Health, and Grocery</option>
                            <option value="Clothing and Accessories">Clothing and Accessories</option>
                            <option value="Books, Sports, and Hobbies">Books, Sports, and Hobbies</option>
                            <option value="Baby Stuffs and Toys">Baby Stuffs and Toys</option>
                            <option value="Real Estate">Real Estate</option>
                            <option value="Cars and Automotives">Cars and Automotives</option>
                            <option value="Motorcycles and Scooters">Motorcycles and Scooters</option>
                            <option value="Services Jobs">Services Jobs</option>
                            <option value="Business and Earning Opportunities">Business and Earning Opportunities</option>
                            <option value="Construction and Farming">Construction and Farming</option>
                            <option value="Pets and Animals">Pets and Animals</option>
                            <option value="Heavy Machines and Trucks">Heavy Machines and Trucks</option>
                        </select>
	                        <h5><span style="color:red">*</span> If you will not update the category, please select your current selected category in the dropdown above.</h5>
                    </div>
                    <div class="col-md-12">
                        <h3>Price & Inventory</h3>
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>Price</h5>
                        <input type="text" class="form-control" name="txtPrice" id="txtPrice" placeholder="Price" required size="1" value='.$price.'>
                    </div>
                    <div class="col-md-12">
                        <h3>Location & Contact Number</h3>
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>Location</h5>
                        <input type="text" class="form-control" name="txtLocation" id="txtLocation" placeholder="Location" required size="1" value="'.$location.'">
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>Contact Number</h5>
                        <input type="text" class="form-control" name="txtMobile" id="txtMobile" placeholder="Contact Number" required size="1" value="'.$mobile.'">
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>Optional:</h5>
                        <input type="text" class="form-control" name="txtMobile2" id="txtMobile2" placeholder="Contact Number #2" size="1" value="'.$mobile2.'">
                        <input type="text" class="form-control" name="txtMobile3" id="txtMobile3" placeholder="Contact Number #3" size="1" value="'.$mobile3.'">
                        <input type="text" class="form-control" name="txtMobile4" id="txtMobile4" placeholder="Contact Number #4" size="1" value="'.$mobile4.'">
                    </div>
                    <div class="form-group col-md-8 col-md-offset-2">
                        <h5>If the product has been sold, update your product status by selecting yes.</h5>
                        <select class="form-control" name="cbSold">
                            <option value= false>No, product not yet sold.</option>
                            <option value= true>Stop selling this product</option>
                            <option value= true>Yes, product has been sold.</option>
                        </select>
                    </div>
                    <div class="text-center" style="margin-top: 1% ">
                        <button style="width: 25%" type="submit" class="btn btn-success btn-md" name="updateproduct"><span class="fa fa-check-circle"></span> Submit</button>
                        <button style="width: 25%" type="button" class="btn btn-danger btn-md"><span class="fa fa-minus-circle"></span> Cancel</button>
                    </div>
                </form>
                </div> 
            </div>
 ';
}

echo

            '
<!-- UPDATE PRODUCT PICTURE MODAL -->                
    <div class="modal fade" id="showPicturesModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Pictures</h3>
                </div>
                    <div class="modal-body">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" style="background-color:rgba(0, 0, 0, 0.8)">
                            <div class="carousel-inner" style="background-color:rgba(0, 0, 0, 0.😎">
                                <div class="item active text-center">
                                    <div style="margin-top:5%">
                                        <img style="width: 480px; height: 480px" src="../uploads/'.$product_picture.'">
                                    </div>
                                </div>
                                ';
                                while ($row = mysqli_fetch_array($modalimages2)){
                                echo
                                '
                                <div class="item text-center">
                                    <div style="margin-top:5%">
                                        <img style="width: 480px; height:480px" src="../uploads/'.$row["product_picture"].'">
                                    </div>
                                </div>
                                ';}
                                echo'
                            </div>
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span style="color:#000080" class="fa fa-arrow-left"></span>
                            <span style="background-color:rgba(255, 255, 255, 0.2)" class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                              <span style="color:#000080" class="fa fa-arrow-right"></span>
                              <span class="sr-only">Next</span>
                        </a>
                    </div>
            </div>

        </div>
    </div> 
';

include('footer.php');