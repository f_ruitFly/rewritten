<?php
include('header.php');
echo
'
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-6 col-md-offset-3" style="background-color: #FFFFFF">
				<div class="text-center"><img src="ASSETS/image/chamee.png" style="width: 200px; height:auto"></div>
				<div class="text-center"><strong>Privacy Policy</strong></div><br>
				
				<strong>Collection</strong><br>
				<p>
				You may browse this Site without providing any personal information about yourself. However, to receive notifications, updates or request additional information about ChamShop. or this Site, we may collect the following information: name, contact information, email address, company and user ID; correspondence sent to or from us; any additional information you choose to provide; and other information from your interaction with our Site, services and content, including computer and connection information, statistics on page views, traffic to and from the Site, ad data, IP address and standard web log information. If you choose to provide us with personal information, you consent to the transfer and storage of that information on our servers located in the United States.
				</p>

				<strong>Use</strong><br>
				<p>
				We use your personal information to provide you with the services you request, communicate with you, troubleshoot problems, customize your experience, inform you about our services and Site updates and measure interest in our sites and services.
				</p>

				<strong>Disclosure</strong><br>
				<p>
				We don\'t sell or rent your personal information to third parties for their marketing purposes without your explicit consent. We may disclose personal information to respond to legal requirements, enforce our policies, respond to claims that a posting or other content violates other\'s rights, or protect anyone\'s rights, property, or safety. Such information will be disclosed in accordance with applicable laws and regulations. We may also share personal information with service providers who help with our business operations, and with members of our corporate family, who may provide joint content and services and help detect and prevent potentially illegal acts. Should we plan to merge or be acquired by another business entity, we may share personal information with the other company and will require that the new combined entity follow this privacy policy with respect to your personal information.<br>
				</p>
				<strong>Security</strong>
				<p>
				We treat information as an asset that must be protected and use lots of tools  to protect your personal information against unauthorized access and disclosure. However, as you probably know, third parties may unlawfully intercept or access transmissions or private communications. Therefore, although we work very hard to protect your privacy, we do not promise, and you should not expect that your personal information or private communications will always remain private.

				</p>

				</div>
			</div>
		</div>
	</div>
';
include('footer.php');