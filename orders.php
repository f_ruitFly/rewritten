<?php
include("header.php");
if (!(isset($_SESSION['pk']))){
header('location: login');
}else{

include ("FUNCTIONS/fProfile.php");

echo
'
<div class="container-fluid">
	<nav class="col-md-2">
		<ul class="nav nav-pills nav-stacked">
			<li>
				<span style="font-weight: 600; font-size: 18px" class="fa fa-user-circle-o"></span>
				<span style="font-weight: 600; font-size: 18px">'.$first_name.' '.$last_name.'</span>
			</li>
			<li><a href="accountsummary?u='.$_SESSION['username'].'" id="sideNavFont">• Account Summary <span class="fa fa-universal-access"></span></a></li>
			<li class="active"><a href="orders?u='.$_SESSION['username'].'" id="sideNavFont">• Orders <span class="fa fa-shopping-basket"></span></a></li>
			<li><a href="personalinformation?u='.$_SESSION['username'].'" id="sideNavFont">• Personal Information <span class="fa fa-info"></span></a></li>
			<li><a href="wishlist?u='.$_SESSION['username'].'" id="sideNavFont">• Wishlist <span class="fa fa-magic"></span></a></li>
			<li><a href="reviews?u='.$_SESSION['username'].'" id="sideNavFont">• My Reviews <span class="fa fa-pencil"></span></a></li>
			<li><a href="addressbook?u='.$_SESSION['username'].'" id="sideNavFont">• Address Book <span class="fa fa-address-book"></span></a></li>
			<li><a href="notificationcenter?u='.$_SESSION['username'].'" id="sideNavFont">• Notification Center <span class="fa fa-exclamation-triangle"></span></a></li>
			<li><a href="message?u='.$_SESSION['username'].'" id="sideNavFont" >• Messages <span class="fa fa-envelope"></span></a></li>
			<li><a href="buyandsell?u='.$_SESSION['username'].'" id="sideNavFont" >• Sell your preloved items <span class="fa fa-shopping-basket"></span></a></li>
			<li><a id="sideNavFont" data-toggle="pill" href="#accountSummary">• Be a seller! <span class="fa fa-briefcase"></span></a></li>
		</ul>
	</nav>
		<div id="orderList">
			<div class="col-md-10">
			<span style="font-size: 20px; font-weight: 600;">Orders</span>
				<div class="row">
					<div class="col-md-12" style="padding-top:10px">
					<span style="padding-right: 3px">Filter and show:</span>
						<select>
							<option>Last 5 items</option>
						</select>
					</div>
				</div>
			</div>
		</div>
';
}