<?php
if ($_GET['p'] == null || $_GET['n'] == null || $_GET['u'] == null){
header('location:../rewritten');
}
else{
include("header.php");
include("FUNCTIONS/fGetItemProducts.php");
echo
'
<script src="JS/order-validation.js"></script>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 10px">
				<div class="col-md-3">
					<a data-toggle="modal" data-target="#showPicturesModal">
						<div>
							<img style="width:300px; height:300px;" src="../uploads/'.$image.'">
						</div>
							';
						while($row = mysqli_fetch_array($getimages)){
						echo
						'	
						 	<img style="width:50px; height:50px;" src="../uploads/'.$row['product_picture'].'">
						';}
					echo
					'
					</a>
				</div>
				<div class="col-md-5 col-md-offset-2">';
				if (isset($_SESSION['success'])){
						echo $_SESSION['success'];
						}
				echo
				'
				</div>
				<div class="col-md-9 line-separator" style="background-color: #FFFFFF;">
					<div class="col-md-12">
						<span style="font-weight: 600; font-size: 24px;">'.$product_name.'</span>
					</div>';
					if ($sale == false){
					echo
					'
					<div class="col-md-12">
						<span style="color: orange; font-size: 20px;">₱'.$price.'</span>
					</div>
					';
					}else{
					echo
					'
					<div class="col-md-12">
						<span style="color:rgba(0,0,0,0.6">'.$totalpercentage.'% off</span>
					</div>
					<div class="col-md-12">
						<span style="color: orange; font-size: 20px;"><del>₱'.$price.'</del> ₱'.$newprice.'</span>
					</div>
					';
					}
					echo
					'
					<div class="col-md-12 text-center">
						<div class="col-md-6 divider-vertical">
							<div>
								<span class="fa fa-heart"></span> 
							</div>
							<div>
								<span> '.$wishlist.' people added this item into their wishlist.</span>
							</div>
						</div>
						<div class="col-md-6">
							<div>
								<span class="fa fa-thumbs-up"></span>
							</div>
							<div>
							 	<span> '.$recommended.' people recommended this item.</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-9 line-separator" style="background-color: #FFFFFF;">
					<div class="col-md-12 text-center" style="margin-top:20px; margin-bottom: 20px">
						<form action="FUNCTIONS/fCart.php?n='.$_GET['n'].'&s='.$_GET['u'].'" method="post">
						<span style="color: rgba(0,0,0,.54)">Quantity:</span>
						<input id="txtQuantity" type="number" style="width: 10%;"  name="txtQuantity" required>
						<span id="stock" style="color: rgba(0,0,0,.54)">'.$stock.' </span><span>pieces available.</span>
						<div style="margin-top:5px">
							<span id="error_message"></span>
						</div>
						<div style="margin-top:5px">
							<button class="shrink" style="background-color: #6FBC92; border-radius; 0px; font-weight:600; color:white; width:250px; height:50px" type="submit" id="addtocart" name="addtocart"><span class="fa fa-cart-plus"></span> ADD TO CART</button>
						</div>
						</form>
					</div>
				</div>
				<div class="col-md-9" style="background-color: #FFFFFF;">
					<div class="col-md-12" style="margin-top:20px; margin-bottom: 20px">
						<div class="col-md-1">
							<img class="img-rounded" style="width: 50px" src="../uploads/'.$shop_image.'">
						</div>
						<div class="col-md-3 divider-vertical">
							<span>'.$store_name.'</span>
							<div style="margin-top: 5px;">';
							if (isset($_SESSION['pk'])){
								if ($follow_added <= 0){
								echo
								'
								<form action="FUNCTIONS/fSellerStats.php?u='.$_SESSION['pk'].'&s='.$sellers_pk.'&b='.$_GET['u'].'&n='.$_GET['n'].'" method="post">
									<button class="shrink" style="background-color: #cccccc; border-radius; 0px; font-weight:600; width:90px;"> <a style="text-decoration:none; color:black;" href="seller?u='.$uploaded_by.'&c=all">View Shop</a></button>
									<button class="shrink" style="background-color: #cccccc; border-radius; 0px; font-weight:600; color:black; width:90px;" type="submit" name="follow"> Follow</button>
								</form>
								';}else{
								echo
								'
								<form action="FUNCTIONS/fSellerStats.php?u='.$_SESSION['pk'].'&s='.$sellers_pk.'" method="post">
									<button class="shrink" style="background-color: #cccccc; border-radius; 0px; font-weight:600; width:90px;"> <a style="text-decoration:none; color:black;" href="seller?u='.$uploaded_by.'&c=all">View Shop</a></button>
									<button class="shrink" style="background-color: #cccccc; border-radius; 0px; font-weight:600; color:black; width:90px;" type="submit" name="unfollow">Unfollow</button>
								</form>
								';}
							}else{
								echo
								'
									<button class="shrink" style="background-color: #cccccc; border-radius; 0px; font-weight:600; width:90px;"> <a style="text-decoration:none; color:black;" href="seller?u='.$uploaded_by.'&c=all">View Shop</a></button>
								';}
							echo 
							'
							</div>
						</div>
						<div class="col-md-8 text-center">
							<div class="col-md-6 divider-vertical">
								<div>
									<span class="fa fa-tags"></span>
								</div>
								<div>
									<span>'.$product_count_result['result'].' products in shop.</span>
								</div>
							</div>
							<div class="col-md-6">
								<div>
									<span class="fa fa-smile-o"></span>
								</div>
								<div>
									<span>'.$positive_rating.' people rated this shop as great.</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
 		<div class="row">
			<div class="col-md-12" style="margin-bottom: 10px">
				<div class="col-md-4" style="margin-bottom: 10px">
					<form action="FUNCTIONS/fItemRatings.php?n='.$_GET['n'].'&u='.$_GET['u'].'" method="post">
					';
				if(isset($_SESSION['pk'])){
					if ($getrecommend_added <= 0){
					echo
					'
						<button class="shrink" style="border:none; background-color:#6FBC92;font-weight:600; color:white;" type="submit" name="recommend"><span class="fa fa-thumbs-up"></span> Recommend this item</button>
					';}else{
					echo
					'
						<button class="shrink" style="border:none; background-color:#6FBC92;font-weight:600; color:white;" type="submit" name="donotrecommend"><span class="fa fa-thumbs-down"></span> Remove recommendation</button>
					';}
					if ($getwishlist_added <= 0){
						echo
						'
						<button class="shrink" style="border:none; background-color:#6FBC92;font-weight:600; color:white;" type="submit" name="addtowishlist"><span class="fa fa-heart"></span> Add to wishlist</button>
						';}else{
						echo
						'
						<button class="shrink" style="border:none; background-color:#6FBC92;font-weight:600; color:white;" type="submit" name="removetowishlist"><span class="fa fa-heart"></span> Remove to wishlist</button>	
						';}}
					echo
					'
					</form>
				</div>
				<div class="col-md-12" style="background-color: #FFFFFF;">
					<div class="col-md-12" style="margin-bottom: 10px">
						<ul class="nav nav-tabs">
							<li class="active" style="font-size: 18px;"><a style="color:black" data-toggle="tab" href="#productDetails">Product Details</a></li>
							<li style="font-size: 18px;"><a style="color:black" data-toggle="tab" href="#comments">Comments ('.$getcommentscount_result['result'].')</a></li>
						</ul>
					</div>
					<div class="tab-content">
						<div class="col-md-6 tab-pane in active" id="productDetails">
							<div class="col-md-12" style="margin-bottom: 20px">
								<span>'.$product_description.'</span>	
							</div>
							<div class="col-md-12">
								<table class="table table-hover table-bordered">
								    <tbody>
								      <tr>
								        <td>Main Category:</td>
								        <td>'.$product_category.'</td>
								      </tr>
								      <tr>
								        <td>Shipping From:</td>
								        <td>'.$sellers_address.'</td>
								      </tr>
								      <tr>
								        <td>Mode of Payment:</td>
								        <td>Bank Deposit</td>
								      </tr>	
								      <tr>
								        <td>Bank Account Name:</td>
								        <td>'.$bank_acc_name.'</td>
								      </tr>
								      <tr>
								        <td>Bank Account Name:</td>
								        <td>'.$bank_name.'</td>
								      </tr>	
								    </tbody>
							    </table>
							    <span class="span-label" style="font-size: 12px">*Account number will be included in the e-mail if you placed the order successfully .</span>
							</div>
						</div>
						<div class="col-md-6 tab-pane" id="comments">
							<div class="col-md-12" style="margin-bottom: 20px">
							';
							if ($getcommentscount_result['result'] < 1){
								echo
								'
									<span> Be the first to comment to this product. </span>
								';
							}else{
								while ($row = mysqli_fetch_array($getcomments)){
								echo
								'
									<div class="col-md-12">
										<span style="font-size: 12px">'.$row['commenter_username'].'</span>
									</div>
									<div class="col-md-12">
										<span style="color: rgba(0,0,0,.54); font-size: 12px">'.$row['date_commented'].' '. $row['time_commented'].'</span>
									</div>
									<div class="col-md-12" style="margin-top: 5px">
										<span style="font-size:14px">'.nl2br($row['comment']).'</span>
									</div>
								';
								}
							}
							echo
							'	
							</div>
							';
							if (isset($_SESSION['pk'])){
							echo
							'
							<div class="col-md-12">
								<form action="FUNCTIONS/fItemRatings.php?n='.$_GET['n'].'&u='.$_GET['u'].'" method="post">
									<textarea class="form-control" name="txtComment" placeholder="Type your desired comment here." required></textarea>
									<button class="shrink pull-right" style="background-color: #6FBC92; border-radius; 0px; font-weight:600; color:white; width:90px; margin-top: 5px; margin-bottom: 10px" type="submit" name="addcomment">Submit</button>
								</form>
							</div>
							';}else{
							echo
							' 
							<div class="col-md-12">
								<span>Please <a href="login">log-in</a> or <a href="registration">sign up</a> to comment.</span>
							</div>
							';}
							echo
							'
						</div>
					</div>
				</div>
			</div>
 		</div>
	</div>

	<div class="container-fluid">
 		<div class="row">
			<div class="col-md-12" style="margin-bottom:10px">
			<a class="pull-right" href="seller?u='.$uploaded_by.'&c=all">SEE MORE</a>
				<div class="col-md-12" style="background-color: #FFFFFF;">
					<div class="col-md-12">
						<span style="font-size: 24px;">From the Same Shop</span>
					</div>
					<div class="col-md-12">
						<div class="col-md-12" style="margin-bottom: 20px">';
						while ($row = mysqli_fetch_array($getshopitems)){
							echo
							'
							<a style="text-decoration:none; color:black" href="item?p='.$row['product_name'].'&n='.$row['pk'].'&u='.$row['uploaded_by'].'">
							<div class="col-md-2 shrink">
								<div style="margin-top: 5px;" class="text-center">
									<img style="width: 120px; height: 120px" src="../uploads/'.$row['product_picture'].'">
								</div>
								<div class="text-center">
									<span>'.substr($row['product_name'],0,15).'</span>
								</div>
								<div>';
								if ($row['sale'] == false){
									echo
									'
									<span style="font-size: 12px; color: orange">₱ '.$row['price'].'</span>
									';
								}else{
									echo
									'
									<span style="font-size: 12px; color: orange"><del>₱ '.$row['price'].'</del> ₱ '.$row['new_price'].'</span>
									';
								}
								echo
								'
									<span class="pull-right" style="color: rgba(0,0,0,.54); font-size: 12px;">Stock: '.$row['stock'].' </span>
								</div>
								<div>
									<span style="color: rgba(0,0,0,.54); font-size: 12px;"><span class="fa fa-heart-o"></span> '.$row['wishlist'].'</span>
									<span class="pull-right" style="color: rgba(0,0,0,.54); font-size: 12px;"><span class="fa fa-thumbs-up"></span> '.$row['recommended'].'</span>
								</div>
							</div>
						</a>';}
						echo
						'
						</div>
					</div>
				</div>
			</div>
 		</div>
	</div>

		<div class="container-fluid">
 		<div class="row">
			<div class="col-md-12">
			<a class="pull-right">SEE MORE</a>
				<div class="col-md-12" style="background-color: #FFFFFF;">
					<div class="col-md-12">
						<span style="font-size: 24px;">More '.$product_category.'</span>
					</div>
					<div class="col-md-12">
						<div class="col-md-12" style="margin-bottom: 20px">';
						while ($row = mysqli_fetch_array($getcategoryitems)){
							echo
							'
							<a style="text-decoration:none; color:black" href="item?p='.$row['product_name'].'&n='.$row['pk'].'&u='.$row['uploaded_by'].'">
							<div class="col-md-2 shrink">
								<div style="margin-top: 5px;" class="text-center">
									<img style="width: 120px; height: 120px" src="../uploads/'.$row['product_picture'].'">
								</div>
								<div class="text-center">
									<span>'.substr($row['product_name'],0,15).'</span>
								</div>
								<div>
									<span style="font-size: 12px; color: orange">₱ '.$row['price'].'</span>
									<span class="pull-right" style="color: rgba(0,0,0,.54); font-size: 12px;">Stock: '.$row['stock'].' </span>
								</div>
								<div>
									<span style="color: rgba(0,0,0,.54); font-size: 12px;"><span class="fa fa-heart-o"></span> '.$row['wishlist'].'</span>
									<span class="pull-right" style="color: rgba(0,0,0,.54); font-size: 12px;"><span class="fa fa-thumbs-up"></span> '.$row['recommended'].'</span>
								</div>
							</div>
						</a>';}
						echo
						'
						</div>
					</div>
				</div>
			</div>
 		</div>
	</div>
';



// wishlist
// happyface
// sadface
// thumbsup
// thumbsdown

echo
            '
<!-- UPDATE PRODUCT PICTURE MODAL -->                
    <div class="modal fade" id="showPicturesModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Pictures</h3>
                </div>
                    <div class="modal-body">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" style="background-color:rgba(0, 0, 0, 0.😎">
                                <div class="item active text-center">
                                    <div style="margin-top:5%">
                                        <img style="width: 480px; height: 480px" src="../uploads/'.$product_picture.'">
                                    </div>
                                </div>
                                ';
                                while ($row = mysqli_fetch_array($modalimages2)){
                                echo
                                '
                                <div class="item text-center">
                                    <div style="margin-top:5%">
                                        <img style="width: 480px; height:480px" src="../uploads/'.$row["product_picture"].'">
                                    </div>
                                </div>
                                ';}
                                echo'
                            </div>
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span style="color:#000080" class="fa fa-arrow-left"></span>
                            <span style="background-color:rgba(255, 255, 255, 0.2)" class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                              <span style="color:#000080" class="fa fa-arrow-right"></span>
                              <span class="sr-only">Next</span>
                        </a>
                    </div>
            </div>
        </div>
    </div>
';
}