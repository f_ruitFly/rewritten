<?php
echo
'
<!DOCTYPE html>
<html>
<head>
    <title>Chamshop | Adapting to your needs</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- BOOTSTRAP -->
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="ASSETS/BOOTSTRAP/CSS/bootstrap-3.3.7.min.css">
    <link rel="stylesheet" type="text/css" href="ASSETS/DIST/font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- FONTS-->
    
    <!-- JS -->
    <script src="ASSETS/JQUERY/jquery-3.2.1.js"></script>
    <script src="ASSETS/BOOTSTRAP/JS/bootstrap-3.3.7.min.js"></script>

    <!-- CUSTOM JS -->
    
    <!-- CUSTOM DESIGNS -->
    <link rel="stylesheet" type="text/css" href="ASSETS/CSS/navbar.css">
    <link rel="stylesheet" type="text/css" href="ASSETS/CSS/profile.css">
    <link rel="stylesheet" type="text/css" href="ASSETS/CSS/frontend.css">
</head>
';

/*** INCLUDES ***/
include ('FUNCTIONS/fGetSessions.php');
include_once('FUNCTIONS/fCartContents.php');
// IF SESSION IS PRESENT, THEN THIS NAVBAR WILL SHOW.
if (isset($_SESSION['pk'])){
echo
'
<body style="background-color:#F1F1F1">
    <nav class="navbar navbar-default">
        <div class="container-fluid" style="background-color: #0D1D32;">
            <div class="navbar-header">
                <a class="navbar-brand" href="../rewritten/"><img id="chamLogo" src="ASSETS/IMAGE/tiange.png" style= "width:140px; height: auto; float:left">
                </a>
            </div>
            <ul class="nav navbar-nav navbar-right" style="margin-top:">
                <li>
                    <a id="navText" class="btn" type="button" href="cart?u='.$_COOKIE['unique'].'">
                        <span style="font-size:26px;" class="fa fa-shopping-cart">
                            <span style="font-size:10px" class="label label-danger">
                                '.$cart_total.'
                            </span>
                        </span>
                    </a>
                </li>
                <li class="dropdown">
                    <a id="navText" class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                        <span style="font-size:26px;" class="fa fa-bell">
                        </span>
                    </a>
                    <ul class="dropdown-menu"style="width: 400px">';
                    $username = $_SESSION['username'];
                    $getnotifications = mysqli_query($connect,"SELECT * FROM notifications WHERE username = '$username' ORDER BY pk DESC LIMIT 5");
                    while ($row = mysqli_fetch_array($getnotifications)){
                        echo
                        '
                        <div class="col-md-12">
                            <li><span>'.$row['message'].'</span></li>
                        </div>

                        ';
                    }
                    echo
                    '
                    <div class="col-md-12 text-center">
                        <li class="divider"></li>
                        <li><a href="notificationcenter">See all notifications</a></li>
                    </div>
                    </ul>
                </li>

                <li class="dropdown">
                    <a id="navText" class="btn dropdown-toggle" style="font-size: 18px" type="button" data-toggle="dropdown">
                        '.$_SESSION['firstname'].' '.$_SESSION['lastname'].' <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                        <a class="btn btn-default btn-sm" style="width: 100%; height: 100%; border-radius: 0" href="personalinformation?u='.$_SESSION['username'].'">
                            <span style="font-weight: 800">
                                <span class="fa fa-user-circle-o">
                                </span> 
                                <br>Profile
                            </span>
                        </a>
                        </li>
                        <li class="divider"></li>
                        <form action="FUNCTIONS/fLogin.php" method="POST">
                            <li>
                                <button class="btn btn-danger btn-sm" style="width: 100%; height: 100%; border-radius: 0" type="submit" name="logout">
                                    <span style="font-weight: 800">
                                        <span class="fa fa-power-off">
                                        </span> 
                                        <br>Log out
                                    </span>
                                </button>
                            </li>
                        </form>                
                    </ul>
                </li> 
            </ul> 
        </div>
         <div class="container-fluid" style="background-color: #0D1D32;">
            <form action="search?" class="navbar-form text-center">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" name="category" size="50">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class="fa fa-search"></span> Search
                  </button>
                </div>
              </div>
            </form>
        </div>
       <div class="container-fluid" style="background-color: #006666; height: 30px">
            <ul class="nav navbar-nav" style="margin-top: -10px">
                <li>
                    <a id="navText" class="btn" type="button" href="../rewritten2">
                        <span>Seller Hub</span>
                    </a>
                </li>
            </ul> 
            <ul class="nav navbar-nav navbar-right" style="margin-top: -10px">
                <li>
                    <a id="navText" class="btn" type="button" href="../rewritten4">
                        <span>Buy and Sell</span>
                    </a>
                </li>
                <li>
                     <a id="navText" class="btn" type="button" href="../rewritten5">
                        <span>Bidding</span>
                    </a>
                </li>
            </ul> 
        </div>
    </nav>

</body>
';

// ELSE IF SESSION IS NOT PRESENT, THEN THIS NAVBAR WILL SHOW.
} else {
echo
'
<body style="background-color:#F1F1F1">
    <nav class="navbar navbar-default">
        <div class="container-fluid" style="background-color: #0D1D32;">
            <div class="navbar-header">
                <a class="navbar-brand" href="../rewritten/"><img id="chamLogo" src="ASSETS/IMAGE/tiange.png" style= "width:140px; height: auto; float:left">
                </a>
            </div>
        <ul class="nav navbar-nav navbar-right" style="margin-top: -10px">
                <li>
                    <a id="navText" class="btn" type="button" href="cart?u='.$_COOKIE['unique'].'">
                        <span style="font-size:26px;" class="fa fa-shopping-cart">
                            <span style="font-size:10px" class="label label-danger">
                                '.$cart_total.'
                            </span>
                        </span>
                    </a>
                </li>

                <li>
                    <a id="navText" style="font-size: 18px" href="registration">Signup</a>
                </li>

                <li>
                    <a id="navText" style="font-size: 18px" href="login">Log in</a>
                </li>
            </ul> 
        </div>
          <div class="container-fluid" style="background-color: #0D1D32;">
            <form action="search?" class="navbar-form text-center">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" name="category" size="50">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <span class="fa fa-search"></span> Search
                  </button>
                </div>
              </div>
            </form>
        </div>
       <div class="container-fluid" style="background-color: #006666; height: 30px">
           <ul class="nav navbar-nav" style="margin-top: -10px">
                <li>
                    <a id="navText" class="btn" type="button" href="../rewritten2">
                        <span>Seller Hub</span>
                    </a>
                </li>
            </ul> 
            <ul class="nav navbar-nav navbar-right" style="margin-top: -10px">
                <li>
                    <a id="navText" class="btn" type="button" href="../rewritten4">
                        <span>Buy and Sell</span>
                    </a>
                </li>
                <li>
                     <a id="navText" class="btn" type="button" href="../rewritten5">
                        <span>Bidding</span>
                    </a>
                </li>
            </ul> 
        </div>
    </nav>
</body>
';

}
