$(document).ready(function(){
	
	var stocks = parseInt($("#stock").text());
	$("#txtQuantity").keydown(function(){
		var quantity = $("#txtQuantity").val();
		if (quantity > stocks){
			$("#error_message").html("Input amount lower than or equal the stocks.");
			$("#error_message").addClass("invalid");
			$("#txtQuantity").val("");
		}
	});

	$("#txtQuantity").mouseleave(function(){
		var quantity = $("#txtQuantity").val();
		if (quantity > stocks){
			$("#error_message").html("Input amount lower than or equal the stocks.");
			$("#error_message").addClass("invalid");
			$("#txtQuantity").val("");
		}
	});

	$("#txtQuantity").blur(function(){
		var quantity = $("#txtQuantity").val();
		if (quantity > stocks){
			$("#error_message").html("Input amount lower than or equal the stocks.");
			$("#error_message").addClass("invalid");
			$("#txtQuantity").val("");
		}
	});

});