$(document).ready(function() {
    $("#txtMobile").keydown(function(event) {
        // Allow: backspace, delete, tab, escape, and enter
        if  (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });

    $("#txtMobile").keydown(function(){
        var mobile = $("#txtMobile").val();
        $.post("JS/AJAX/form-validation.php",{
            mobile:mobile
        },
        function(data,status){
        var mobile_length = $("#txtMobile").val().length;
            if (mobile_length !== 11){
            $("#status_mobile").addClass("invalid");
            $("#status_mobile").html("Mobile number should be 11 numbers only.");
            $("#register").hide();  
            }else{
            $("#status_mobile").html(data);
            $("#register").show();
                if ($("#status_mobile").text() == "Mobile number is already taken!"){
                    $("#register").hide();

                }
            }
        });
    });

    $("#txtUsername").keydown(function(){
        var username = $("#txtUsername").val();
        $.post("JS/AJAX/form-validation.php",{
            username:username
        },
        function(data,status){
        var username_length = $("#txtUsername").val().length;
            if (username_length < 5 || username_length > 20){
            $("#status_username").addClass("invalid");
            $("#status_username").html("Username should have letters and numbers, and it should be between 5-20 characters only.");
            $("#register").hide();  
            }else{
            $("#status_username").html(data);
            $("#register").show();
                if ($("#status_username").text() == "Username is already taken!"){
                    $("#register").hide();

                }
            }
        });
    });

    $("#txtEmail").keydown(function(){
        var email = $("#txtEmail").val();
        $.post("JS/AJAX/form-validation.php",{
            email:email
        },
        function(data,status){
            $("#status_email").html(data);
            $("#register").show();
                if ($("#status_email").text() == "E-mail address is already taken!"){
                    $("#register").hide();

                }
            });
    });
    
     $("#txtPassword").keydown(function(){
        var password = $("#txtPassword").val().length;
        if (password < 5){
           $("#status_password").html("Your password is too short.");
           $("#status_password").removeClass("valid");
           $("#status_password").addClass("invalid");
           $("#register").hide();
        }else if (password > 5){
           $("#status_password").html("Your password is okay.");
           $("#status_password").removeClass("invalid");
           $("#status_password").addClass("valid");
           $("#register").show();
        }
    });

        $("#forgotemail").mouseleave(function(){
        var forgotemail = $("#forgotemail").val();
        var rdbtype = $("input[name=rdbType]:checked").val();
        $.post("JS/AJAX/form-validation.php",{
            forgotemail:forgotemail,
            rdbtype: rdbtype
           
        },
        function(data,status){
            $("#status_email").html(data);
            $("#register").hide();
                if ($("#status_email").text() == "E-mail address matched to an account."){
                    $("#register").show();

                }
            });
    });
});

