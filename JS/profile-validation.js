$(document).ready(function() {
	$("#savePassword").hide();
	$("#txtCurrentPassword").blur(function(){
		var currentpassword = $("#txtCurrentPassword").val();
		$.get("JS/AJAX/personalinformation-validation.php",
		{
			currentpassword : currentpassword
		},function(data,result){
			$("#error_current_password").html(data);
		});
	});

	$("#txtNewPassword").mouseleave(function(){
		var newpassword = $("#txtNewPassword").val().length;
		if (newpassword < 5){
		   $("#error_new_password").html("Your password is too short.");
		   $("#error_new_password").removeClass("valid");
		   $("#error_new_password").addClass("invalid");
		   $("#savePassword").hide();
		}else if (newpassword > 5){
		   $("#error_new_password").html("Your password is okay.");
		   $("#error_new_password").removeClass("invalid");
		   $("#error_new_password").addClass("valid");
		   $("#savePassword").show();
		}
	});

	$("#txtRetypePassword").mouseleave(function(){
		var newpassword = $("#txtNewPassword").val();
		var retypepassword = $("#txtRetypePassword").val();
		if (newpassword != retypepassword){
		   $("#error_retype_password").html("Your current and retyped password did not match.");
		   $("#error_retype_password").removeClass("valid");
		   $("#error_retype_password").addClass("invalid");
		   $("#savePassword").hide();		
		}else if (newpassword == retypepassword){
		   $("#error_retype_password").html("Current and retyped password matched.");
		   $("#error_retype_password").removeClass("invalid");
		   $("#error_retype_password").addClass("valid");
		   $("#savePassword").show();		
		}
	});

	$("#txtEmail").keyup(function(){
        var email = $("#txtEmail").val();
        $.post("JS/AJAX/form-validation.php",{
            email:email
        },
        function(data,status){
            $("#status_email").html(data);
            $("#register").show();
                if ($("#status_email").text() == "E-mail address is already taken."){
                    $("#register").hide();
                }
            });
    });
});