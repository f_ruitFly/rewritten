<?php

echo
'
	<footer class="navbar navbar-default">
		<div class="container-fluid footer-color">
			<div class="col-md-3">
				<div class="col-md-12">
					<div>
						<span class="chamshop-left">ChamShop</span>
					</div>
					<div class="col-md-12 chamshop-margin-top">
						<div>
							<a href="aboutus" class="chamshop-left-a">Abous Us</span>
						</div>
						<div>
							<a href="termsofuse" class="chamshop-left-a">Terms of Use</span>
						</div>
						<div>
							<a href="privacypolicy" class="chamshop-left-a">Privacy Policy</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="col-md-12">
					<div>
						<span class="chamshop-left">Contact Us</span>
					</div>
					<div class="col-md-12 chamshop-margin-top">
						<div>
							<a class="chamshop-left-a">Phone: 09265504274</span>
						</div>
						<div>
							<a class="chamshop-left-a">Email: champshopph@gmail.com</span>
						</div>
					</div>
				</div>
				<div class="chamshop-vertical-divider"></div>
			</div>
			<div class="col-md-3">
				<div class="col-md-12">
					<a><img class="chamshop-social-media" src="ASSETS/IMAGE/steam.png"></a>
					<a><img class="chamshop-social-media" src="ASSETS/IMAGE/bitbucket.png"></a>
					<a><img class="chamshop-social-media" src="ASSETS/IMAGE/facebook.png"></a>
					<a><img class="chamshop-social-media" src="ASSETS/IMAGE/twitter.png"></a>
				</div>
				<div class="chamshop-vertical-divider"></div>
			</div>
			<div class="col-md-3">
				<div class="col-md-12">
					<a><img class="chamshop-logo" src="ASSETS/IMAGE/official_logo.png"></a>
				</div>
				<div class="col-md-12 text-center">
					<span class="chamshop-founders">Ezekiel</span>
					<span class="chamshop-founders">JohnCarlo</span>
					<span class="chamshop-founders">Reginald</span>
					<span class="chamshop-founders">Marhea</span>
					<span class="chamshop-founders">Genesis</span>		
				</div>
				
				<div class="chamshop-vertical-divider"></div>
			</div>
		</div>
		<div class="container-fluid footer-color-2">
			<div class="col-md-12">
				<div class="col-md-12 text-center chamshop-copyright">
					<span>©ChamShop 2018</span>
				</div>
			</div>
		</div>
	</footer>
';