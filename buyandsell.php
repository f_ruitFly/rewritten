<?php
include("header.php");
if (!(isset($_SESSION['pk']))){
header('location: login');
}else{

include_once("FUNCTIONS/fProfile.php");
include_once("FUNCTIONS/fGetBuyAndSell.php");
echo
'
<div class="container-fluid">
	<nav class="col-md-2">
		<ul class="nav nav-pills nav-stacked">
			<li>
				<span style="font-weight: 600; font-size: 18px" class="fa fa-user-circle-o"></span>
				<span style="font-weight: 600; font-size: 18px">'.$first_name.' '.$last_name.'</span>
			</li>
			<li><a href="personalinformation?u='.$_SESSION['username'].'" id="sideNavFont">• Personal Information <span class="fa fa-info"></span></a></li>
			<li><a href="wishlist?u='.$_SESSION['username'].'" id="sideNavFont">• Wishlist <span class="fa fa-magic"></span></a></li>
			<li><a href="reviews?u='.$_SESSION['username'].'" id="sideNavFont">• My Reviews <span class="fa fa-pencil"></span></a></li>
			<li><a href="addressbook?u='.$_SESSION['username'].'" id="sideNavFont">• Address Book <span class="fa fa-address-book"></span></a></li>
			<li><a href="notificationcenter?u='.$_SESSION['username'].'" id="sideNavFont">• Notification Center <span class="fa fa-exclamation-triangle"></span></a></li>
			<li class="active"><a href="buyandsell?u='.$_SESSION['username'].'" id="sideNavFont" >• Sell your preloved items <span class="fa fa-shopping-basket"></span></a></li>
			<li><a id="sideNavFont" href="../rewritten2/registration">• Be a seller! <span class="fa fa-briefcase"></span></a></li>
		</ul>
	</nav>

	<div id="notificationCenter">
		<div class="col-md-10">
			<span style="font-size: 20px; font-weight: 600;">Your preloved items</span>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-12">
							<a href="addproduct"><span>Sell your products</span></a>
						</div>
						<div class="col-md-8 col-md-offset-2">';
							if (isset($_SESSION['success'])){
								echo $_SESSION['success'];
							}
						echo';
						</div>
						<div class="col-md-12 div-margin-top div-margin-bottom">';
						while ($row = mysqli_fetch_array($getitems)){
							echo
							'
							<a href="manage?p='.$row['pk'].'&u='.$row['uploaded_by'].'&n='.$row['product_name'].'" class="ignore-a">
								<div class="col-md-3 chamshop-categories shrink" style="cursor:pointer">
									<div class="col-md-12 text-center small-padding">
										<img style="width: 100px; height: 100px" src="../uploads/'.$row['product_picture'].'">
									</div>
									<div class="col-md-12 text-center">
										<span>'.substr($row['product_name'], 0, 25).'</span>
									</div>
									<div class="col-md-12 text-center">
										<span>Price: '.$row['price'].'</span>
									</div>
								</div>
							</a>
							'
						;}
						echo
						'
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
';

include('footer.php');
}