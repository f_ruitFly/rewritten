<?php
include('header.php');
echo
'
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-6 col-md-offset-3" style="background-color: #FFFFFF">
				<div class="text-center"><img src="ASSETS/image/chamee.png" style="width: 200px; height:auto"></div>
				<div class="text-center"><strong>About Chamshop</strong></div><br>
				<p>ChamShop - an online shopping and bidding website that will cater online shopping needs of everyone. Its primary target in the market are the small to medium scale businesses, people who sell their products on social media like Facebook and Instagram, either preloved or brand new, local or imported. This will also allow product bidding to its customers. <br><br>
				 ChamShop has no control over and does not guarantee the existence, quality, safety or legality of items advertised; the truth or accuracy of users\' content or listings; the ability of sellers to sell items; the ability of buyers to pay for items; or that a buyer or seller will actually complete a transaction or return an item.</p>
				 <div class="text-center"><img src="ASSETS/image/Buyer hub.png" style="width: 200px; height:auto"></div>
				<div class="text-center"><strong>About Us</strong></div><br>
				<div class="text-center">
					<p>
					Ezekiel Josue Alera<br>
					JohnCarlo Dela Cruz<br>
					Reginald Lanozo<br>
					Marhea San Antonio<br>
					Genesis Sanggalang
					<br><br>
					</p>
				</div>
					<p>
					We are group of students who are dreaming awake, laughing, relaxing, and playing all the time. But when January 2018 hits, we decided to conduct a challenge called 1-month thesis challenge. It\'s one hell of a ride.
					</p>
				</div>
			</div>
		</div>
	</div>
';
include('footer.php');