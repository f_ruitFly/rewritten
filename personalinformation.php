<?php
include("header.php");
if (!(isset($_SESSION['pk']))){
header('location: login');
}else{

include ("FUNCTIONS/fProfile.php");
echo
'
<script src="JS/profile-validation.js"></script>
<div class="container-fluid">
	<nav class="col-md-2">
		<ul class="nav nav-pills nav-stacked">
			<li>
				<span style="font-weight: 600; font-size: 18px" class="fa fa-user-circle-o"></span>
				<span style="font-weight: 600; font-size: 18px">'.$first_name.' '.$last_name.'</span>
			</li>
			<li class="active"><a href="personalinformation?u='.$_SESSION['username'].'" id="sideNavFont">• Personal Information <span class="fa fa-info"></span></a></li>
			<li><a href="wishlist?u='.$_SESSION['username'].'" id="sideNavFont">• Wishlist <span class="fa fa-magic"></span></a></li>
			<li><a href="reviews?u='.$_SESSION['username'].'" id="sideNavFont">• My Reviews <span class="fa fa-pencil"></span></a></li>
			<li><a href="addressbook?u='.$_SESSION['username'].'" id="sideNavFont">• Address Book <span class="fa fa-address-book"></span></a></li>
			<li><a href="notificationcenter?u='.$_SESSION['username'].'" id="sideNavFont">• Notification Center <span class="fa fa-exclamation-triangle"></span></a></li>
			<li><a href="buyandsell?u='.$_SESSION['username'].'" id="sideNavFont" >• Sell your preloved items <span class="fa fa-shopping-basket"></span></a></li>
			<li><a id="sideNavFont" href="../rewritten2/registration">• Be a seller! <span class="fa fa-briefcase"></span></a></li>
		</ul>
	</nav>
		<div class="col-md-10">
			<span style="font-size: 20px; font-weight: 600;">Personal Information</span>
				<div class="row">
					<div class="col-md-12" style="padding-top:10px">
						<div class="col-md-12" style="border: solid 1px #AAAAAA; background-color: #dddddd; padding:10px;">
							<div class="col-md-6">
								<div>
									<span class="fa fa-user-circle-o"></span> <label>Full Name</label>
								</div>
								<div>
									<span style="padding-left: 30px">'.$first_name.' '.$last_name.'</span>
								</div>
								<div style="padding-top:10px">
									<span class="fa fa-user-secret"></span> <label>Username</label>
								</div>
								<div>
									<span style="padding-left: 30px">'.$username.'</span>
								</div>
								<div style="padding-top: 10px">
									<span class="fa fa-envelope"></span> <label>E-mail</label>
								</div>
								<div>
									<span style="padding-left: 30px">'.$email.'</span>
								</div>
								<div style="padding-top:20px">
									<a class="btn btn-default" data-toggle="modal" data-target="#updateEmail" style="margin-left: 30px">CHANGE E-MAIL</a>
								</div>
								<div>
									<a class="btn btn-default" data-toggle="modal" data-target="#updatePassword" style="margin-left: 30px">CHANGE PASSWORD</a>
								</div>
							</div>
							<div class="col-md-6">
								<div>
								<span class="fa fa-mobile" style="font-size:20px"></span> <label>Mobile Number</label>
								</div>
								<div>
									<span style="padding-left: 30px">'.$mobile.'</span>
								</div>
								<div style="padding-top:20px">
									<a class="btn btn-default" data-toggle="modal" data-target="#updateInformationModal" style="margin-left: 30px">UPDATE INFORMATION</a>
								</div>
							</div>	
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
';
}

// MODAL START
// UPDATE INFORMATION
echo 
'
 <div class="modal fade" id="updateInformationModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-title">
                	<div class="text-center">
						<img class="img-rounded" style="width: 180px; height:auto" src="ASSETS/IMAGE/chamee.png">
	            		<h3 style="font-weight:600;">Update Information</h3>
            		</div>
	            		<h5 style="padding-top:5px;">I`m <span style="font-weight:800">Huni! </span>Your friendly chameleon.</h5>
	            		<h5>Please do not leave the the fields blank. Thank you!</h5>
                </div>
            </div>
            <div class="modal-body">
	            <form autocomplete="off" style="margin-top:%" action="FUNCTIONS/fProfile.php?u='.$_SESSION['username'].'" method="post">
	            <span class="fa fa-user-circle-o"></span> <label>Full Name *</label>
					<div class="form-inline">
						<div class="form-group">
							<input type="text" class="form-control" name="txtFirstName" id="txtFirstName" value="'.$first_name.'" placeholder="Enter First Name" required>
							<input type="text" class="form-control" name="txtLastName" id="txtLastName" value="'.$last_name.'" placeholder="Enter Last Name" required>
						</div>
					</div>
						<div class="form-group">
							<span class="fa fa-mobile" style="font-size:20px"></span> <label for="txtMobile">Mobile Number *</label>
							<input style="width:55%" type="text" class="form-control" name="txtMobile" id="txtMobile" value="'.$mobile.'" placeholder="Enter your mobile number" required>
						</div>
						<div class="btn-group">
							<div class="btn-group">
								<button class="btn btn-success" name="saveUpdate" type="submit">Update </button>
							</div>
							<div class="btn-group">
								<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel </button>
							</div>
						</div>
	            </form>
            </div>
        </div>
    </div>
</div>
';

// UPDATE EMAIL
echo
'
 <div class="modal fade" id="updateEmail" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-title">
                	<div class="text-center">
						<img class="img-rounded" style="width: 180px; height:auto" src="ASSETS/IMAGE/chamee.png">
	            		<h3 style="font-weight:600;">Update Information</h3>
            		</div>
	            		<h5 style="padding-top:5px;">I`m <span style="font-weight:800">Huni! </span>Your friendly chameleon.</h5>
	            		<h5>Please do not leave the the fields blank. Thank you!</h5>
                </div>
            </div>
            <div class="modal-body">
	            <form autocomplete="off" style="margin-top:%" action="FUNCTIONS/fProfile.php?u='.$_SESSION['username'].'" method="post">
	            <div class="form-group">
					<span>Your current e-mail address: </span>
						<div>
							<label>'.$email.'</label>
						</div>
				</div>
	            <div class="form-group">
					<span class="fa fa-envelope"></span> <label for="txtEmail">E-mail *</label>
						<input style="width:55%" type="email" class="form-control" name="txtEmail" id="txtEmail" placeholder="example@email.com" required>
						<div>
							<span id="status_email"></span>
						</div>
				</div>
						<div class="btn-group">
							<div class="btn-group">
								<button class="btn btn-success" name="saveEmail" type="submit">Update </button>
							</div>
							<div class="btn-group">
								<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel </button>
							</div>
						</div>
	            </form>
            </div>
        </div>
    </div>
</div>
';

// CHANGE PASSWORD MODAL
echo
'
 <div class="modal fade" id="updatePassword" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-title">
                	<div class="text-center">
						<img class="img-rounded" style="width: 180px; height:auto" src="ASSETS/IMAGE/chamee.png">
	            		<h3 style="font-weight:600;">Update Information</h3>
            		</div>
	            		<h5 style="padding-top:5px;">I`m <span style="font-weight:800">Huni! </span>Your friendly chameleon.</h5>
	            		<h5>Please do not leave the the fields blank. Thank you!</h5>
                </div>
            </div>
            <div class="modal-body">
	            <form autocomplete="off" style="margin-top:%" action="FUNCTIONS/fProfile.php?u='.$_SESSION['username'].'" method="post">
	            <div class="form-group">
					<label for="txtCurrentPassword">Current Password: </label>
						<input type="password" class="form-control" name="txtCurrentPassword" id="txtCurrentPassword" placeholder="Your current password" required size="40">
						<span id="error_current_password"></span>
				</div>
				<div class="form-group">
					<label for="txtNewPassword">New Password: </label>
						<input type="password" class="form-control" name="txtNewPassword" id="txtNewPassword" placeholder="Your new password" required size="40">
						<span id="error_new_password"></span>
				</div>
				<div class="form-group">
					<label for="txtRetype">Re-type New Password: </label>
						<input type="password" class="form-control" name="txtRetypePassword" id="txtRetypePassword" placeholder="Re-type your new password" required size="40">
						<span id="error_retype_password"></span>
				</div>
						<div class="btn-group">
							<div class="btn-group">
								<button class="btn btn-success" name="savePassword" type="submit" id="savePassword">Update</button>
							</div>
							<div class="btn-group">
								<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel </button>
							</div>
						</div>
	            </form>
            </div>
        </div>
    </div>
</div>
';
include('footer.php');