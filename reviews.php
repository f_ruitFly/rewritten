<?php
include("header.php");
if (!(isset($_SESSION['pk']))){
header('location: login');
}else{

include ("FUNCTIONS/fGetReviews.php");

echo
'
<div class="container-fluid">
	<nav class="col-md-2">
		<ul class="nav nav-pills nav-stacked">
			<li>
				<span style="font-weight: 600; font-size: 18px" class="fa fa-user-circle-o"></span>
				<span style="font-weight: 600; font-size: 18px">'.$first_name.' '.$last_name.'</span>
			</li>
			<li><a href="personalinformation?u='.$_SESSION['username'].'" id="sideNavFont">• Personal Information <span class="fa fa-info"></span></a></li>
			<li><a href="wishlist?u='.$_SESSION['username'].'" id="sideNavFont">• Wishlist <span class="fa fa-magic"></span></a></li>
			<li class="active"><a href="reviews?u='.$_SESSION['username'].'" id="sideNavFont">• My Reviews <span class="fa fa-pencil"></span></a></li>
			<li><a href="addressbook?u='.$_SESSION['username'].'" id="sideNavFont">• Address Book <span class="fa fa-address-book"></span></a></li>
			<li><a href="notificationcenter?u='.$_SESSION['username'].'" id="sideNavFont">• Notification Center <span class="fa fa-exclamation-triangle"></span></a></li>
			<li><a href="buyandsell?u='.$_SESSION['username'].'" id="sideNavFont" >• Sell your preloved items <span class="fa fa-shopping-basket"></span></a></li>
			<li><a id="sideNavFont" href="../rewritten2/registration">• Be a seller! <span class="fa fa-briefcase"></span></a></li>
		</ul>
	</nav>
	<div id="myReviews">
		<div class="col-md-10">
			<span style="font-size: 20px; font-weight: 600;">My Reviews</span>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-8">
							<div>
								<span style="font-weight:600">My comments</span>
							</div>';
						while ($row = mysqli_fetch_array($getreviews)){
							$product_pk = $row['product_pk'];
							$getproduct = mysqli_query($connect, "SELECT * FROM sellers_products WHERE pk = $product_pk");
							while ($product = mysqli_fetch_array($getproduct)){
							echo
							'
						<a style="text-decoration:none; color:black;" href="item?p='.$product['product_name'].'&n='.$product['pk'].'&u='.$product['uploaded_by'].'">
							<div class="col-md-12" style="background-color: #FFFFFF; margin-top: 10px">
								<div class="col-md-12">
									<span>'.$product['product_name'].'</span>
								</div>
								<div class="col-md-12">
									<span style="color: rgba(0,0,0,0.6);">'.$row['date_commented'].'</span>
								</div>
								<div class="col-md-12">
									<span style="color: rgba(0,0,0,0.6);">'.$row['comment'].'</span>
								</div>
							</div>
						</a>
						';}}
						echo
						'
						</div>
						<div class="col-md-4">
							<span style="font-weight:600">My Recommendations</span>
							<div class="col-md-12" style="background-color: #FFFFFF; margin-top: 10px">';
							while ($row = mysqli_fetch_array($getrecommendation)){
							$product_pk = $row['product_pk'];
							$getproduct = mysqli_query($connect, "SELECT * FROM sellers_products WHERE pk = $product_pk");
							while ($product = mysqli_fetch_array($getproduct)){
							echo
							'
							<a style="text-decoration:none; color:black;" href="item?p='.$product['product_name'].'&n='.$product['pk'].'&u='.$product['uploaded_by'].'">
								<div class="col-md-12">
									<span>• '.$product['product_name'].'</span>
								</div>
								<div class="col-md-10 col-md-offset-1">
									<span>'.$row['date_recommended'].'</span>
								</div>
							</a>
							';}}
							echo
							'
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
';
}

include('footer.php');