-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2018 at 02:37 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `huni`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `pk` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `tries` int(11) NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `billing_address`
--

CREATE TABLE `billing_address` (
  `pk` int(11) NOT NULL,
  `full_name` text NOT NULL,
  `address` text NOT NULL,
  `province` text NOT NULL,
  `municipality` text NOT NULL,
  `city` text NOT NULL,
  `mobile` text NOT NULL,
  `email` text NOT NULL,
  `account_pk` int(11) NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billing_address`
--

INSERT INTO `billing_address` (`pk`, `full_name`, `address`, `province`, `municipality`, `city`, `mobile`, `email`, `account_pk`, `archived`) VALUES
(19, 'Genesis Sanggalang', '#238 J. Garcia St.', 'Bulacan', 'Plaridel', ' Banga 2nd', '09972736778', 'sanggalanggv@gmail.com', 0, 0),
(20, 'Genesis V. Sanggalang', '#146 Tambubong-Batia Road', 'Bulacan', 'Bocaue', 'Tambubong', '09972736778', 'sanggalanggv@gmail.com', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `pk` int(11) NOT NULL,
  `username` text NOT NULL,
  `uploaded_by` text NOT NULL,
  `product_pk` int(11) NOT NULL,
  `message` text NOT NULL,
  `date_notified` date NOT NULL,
  `time_notified` time NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`pk`, `username`, `uploaded_by`, `product_pk`, `message`, `date_notified`, `time_notified`, `archived`) VALUES
(1, 'aimhighltd', 'aimhighltd', 22, 'commented in your product.', '2018-01-22', '13:44:42', 0),
(2, 'aimhighltd', 'aimhighltd', 22, 'commented in your product.', '2018-01-22', '14:06:43', 0),
(4, 'aimhighltd', 'aimhighltd', 22, 'recommended your product.', '2018-01-22', '14:13:48', 0),
(5, 'aimhighltd', 'aimhighltd', 22, 'added your product in their wishlist.', '2018-01-22', '14:15:12', 0),
(6, 'aimhighltd', 'aimhighltd', 22, 'added your product in their wishlist.', '2018-01-22', '14:15:45', 0),
(7, 'aimhighltd', 'aimhighltd', 0, 'is now following your shop.', '2018-01-22', '14:34:37', 0),
(8, 'aimhighltd', 'aimhighltd', 0, 'is now following your shop.', '2018-01-22', '14:34:52', 0),
(9, 'aimhighltd', 'aimhighltd', 0, 'is now following your shop.', '2018-01-22', '14:36:38', 0),
(10, 'aimhighltd', 'aimhighltd', 0, 'recommended your shop.', '2018-01-22', '14:36:49', 0),
(11, 'aimhighltd', 'aimhighltd', 0, 'is now following your shop.', '2018-01-22', '14:37:03', 0),
(12, 'aimhighltd', '', 0, 'is now following your shop.', '2018-01-22', '14:37:49', 0),
(13, 'aimhighltd', 'aimhighltd', 0, 'is now following your shop.', '2018-01-22', '14:39:22', 0),
(14, 'aimhighltd', 'aimhighltd', 0, 'recommended your shop.', '2018-01-22', '14:40:12', 0),
(15, 'aimhighltd', 'aimhighltd', 0, 'is now following your shop.', '2018-01-22', '14:40:31', 0),
(16, 'sanggalanggv', 'aimhighltd', 0, 'is now following your shop.', '2018-01-24', '15:42:51', 0),
(17, '', 'aimhighltd', 21, 'Check your orders, someone placed an order!', '2018-01-25', '23:08:45', 0),
(18, '', 'aimhighltd', 22, 'Check your orders, someone placed an order!', '2018-01-25', '23:08:45', 0),
(19, '', 'aimhighltd', 23, 'Check your orders, someone placed an order!', '2018-01-25', '23:08:45', 0),
(20, '', 'aimhighltd', 21, 'Check your orders, someone placed an order to your item!', '2018-01-27', '13:54:03', 0),
(21, '', 'aimhighltd', 21, 'Check your orders, someone placed an order to your item!', '2018-01-27', '14:50:12', 0),
(22, '', 'aimhighltd', 22, 'Check your orders, someone placed an order to your item!', '2018-01-27', '14:50:12', 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `pk` int(11) NOT NULL,
  `hash` text NOT NULL,
  `billing_pk` int(11) NOT NULL,
  `shipping_pk` int(11) NOT NULL,
  `user_pk` text NOT NULL,
  `date_placed` date NOT NULL,
  `time_placed` time NOT NULL,
  `approved` text NOT NULL,
  `success` tinyint(1) NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`pk`, `hash`, `billing_pk`, `shipping_pk`, `user_pk`, `date_placed`, `time_placed`, `approved`, `success`, `archived`) VALUES
(21, 'd429c59a6c23d0db9a38b5fc3f38941c', 1, 1, '8', '2018-01-25', '22:24:05', 'yes', 0, 0),
(22, '6196442a05eb918c20874a27d09ed6e4', 14, 14, '23601fc57ec80e135fccbc295716b7ad', '2018-01-26', '15:03:03', 'pending', 0, 0),
(23, 'e77182a48dc5692e6de1270260407945', 14, 17, '258aedeac5b14d71b6311e4f2810614d', '2018-01-26', '15:46:13', 'pending', 0, 0),
(24, '81560da5a458f26399ca2394f75e2077', 17, 18, '21aa0256313f014f11a1fb8d45a91bcc', '2018-01-26', '15:47:24', 'pending', 0, 0),
(25, 'd2106fad2f7db8bd591141cf2f53ef95', 18, 19, '21aa0256313f014f11a1fb8d45a91bcc', '2018-01-26', '15:48:08', 'pending', 0, 0),
(27, 'a56a238ba5eb60372f11c9fd8a75535c', 20, 21, 'e17cb56036d31e7d4c87eed66ae48024', '2018-01-27', '14:49:43', 'yes', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_comments`
--

CREATE TABLE `product_comments` (
  `pk` int(11) NOT NULL,
  `commenter_username` text NOT NULL,
  `product_pk` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_commented` date NOT NULL,
  `time_commented` time NOT NULL,
  `helpful` int(11) NOT NULL,
  `not_helpful` int(11) NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_comments`
--

INSERT INTO `product_comments` (`pk`, `commenter_username`, `product_pk`, `comment`, `date_commented`, `time_commented`, `helpful`, `not_helpful`, `archived`) VALUES
(1, '0', 0, '', '2018-01-19', '00:00:00', 0, 0, 0),
(2, '0', 0, '', '0000-00-00', '00:00:00', 0, 0, 0),
(3, '0', 0, '', '0000-00-00', '17:14:45', 0, 0, 0),
(7, 'sanggalanggv', 21, 'Good\r\n', '2018-01-19', '17:40:52', 0, 0, 0),
(8, 'aimhighltd', 22, 'Beautiful product, krappa G.', '2018-01-22', '13:44:42', 0, 0, 0),
(9, 'aimhighltd', 22, 'Hello.', '2018-01-22', '14:06:43', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_recommendation`
--

CREATE TABLE `product_recommendation` (
  `pk` int(11) NOT NULL,
  `username` text NOT NULL,
  `product_pk` int(11) NOT NULL,
  `date_recommended` date NOT NULL,
  `time_recommended` time NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_recommendation`
--

INSERT INTO `product_recommendation` (`pk`, `username`, `product_pk`, `date_recommended`, `time_recommended`, `archived`) VALUES
(1, 'sanggalanggv', 21, '2018-01-19', '18:23:33', 1),
(2, 'sanggalanggv', 21, '2018-01-19', '18:26:42', 1),
(3, 'sanggalanggv', 21, '2018-01-19', '18:27:27', 1),
(4, 'sanggalanggv', 21, '2018-01-19', '18:30:40', 1),
(5, 'sanggalanggv', 21, '2018-01-19', '18:30:47', 1),
(6, 'sanggalanggv', 21, '2018-01-19', '18:32:30', 1),
(7, 'sanggalanggv', 21, '2018-01-19', '19:57:51', 1),
(8, 'sanggalanggv', 21, '2018-01-19', '19:58:51', 1),
(9, 'sanggalanggv', 21, '2018-01-19', '20:27:55', 1),
(10, 'sanggalanggv', 21, '2018-01-19', '20:28:49', 1),
(11, 'sanggalanggv', 21, '2018-01-19', '20:29:01', 1),
(12, 'sanggalanggv', 21, '2018-01-19', '20:29:27', 1),
(13, 'sanggalanggv', 21, '2018-01-19', '20:32:33', 1),
(14, 'sanggalanggv', 21, '2018-01-19', '20:33:07', 1),
(15, 'sanggalanggv', 21, '2018-01-19', '20:36:08', 1),
(16, 'sanggalanggv', 21, '2018-01-19', '20:42:11', 1),
(17, 'sanggalanggv', 21, '2018-01-20', '12:40:53', 0),
(18, 'aimhighltd', 21, '2018-01-21', '12:44:15', 1),
(19, 'aimhighltd', 22, '2018-01-21', '13:51:58', 1),
(20, 'sanggalanggv', 22, '2018-01-22', '12:21:35', 0),
(21, 'aimhighltd', 22, '2018-01-22', '14:12:27', 1),
(22, 'aimhighltd', 22, '2018-01-22', '14:13:48', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE `sellers` (
  `pk` int(11) NOT NULL,
  `default_picture` text NOT NULL,
  `store_type` text NOT NULL,
  `email` text NOT NULL,
  `store_name` text NOT NULL,
  `mobile` text NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `firstname` text NOT NULL,
  `lastname` text NOT NULL,
  `company_name` text NOT NULL,
  `tin_number` text NOT NULL,
  `birthdate` text NOT NULL,
  `age` text NOT NULL,
  `gender` text NOT NULL,
  `civil_status` text NOT NULL,
  `educ_attainment` text NOT NULL,
  `occupation` text NOT NULL,
  `dependents` text NOT NULL,
  `company_address` text NOT NULL,
  `city_town` text NOT NULL,
  `company_postal` text NOT NULL,
  `company_mobile` text NOT NULL,
  `main_category` text NOT NULL,
  `acc_name` text NOT NULL,
  `acc_number` text NOT NULL,
  `bank_name` text NOT NULL,
  `step1` tinyint(1) NOT NULL,
  `step2` tinyint(1) NOT NULL,
  `step3` tinyint(1) NOT NULL,
  `auth` tinyint(1) NOT NULL,
  `approved` text NOT NULL,
  `followers` int(11) NOT NULL,
  `good_rating` int(11) NOT NULL,
  `bad_rating` int(11) NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sellers`
--

INSERT INTO `sellers` (`pk`, `default_picture`, `store_type`, `email`, `store_name`, `mobile`, `username`, `password`, `firstname`, `lastname`, `company_name`, `tin_number`, `birthdate`, `age`, `gender`, `civil_status`, `educ_attainment`, `occupation`, `dependents`, `company_address`, `city_town`, `company_postal`, `company_mobile`, `main_category`, `acc_name`, `acc_number`, `bank_name`, `step1`, `step2`, `step3`, `auth`, `approved`, `followers`, `good_rating`, `bad_rating`, `archived`) VALUES
(18, 'ChamShop.png', 'Company', 'aimhighltd@gmail.com', 'The Beginning Store', '+639972736678', 'aimhighltd', '$2y$10$FXmObxQMB2pBNbXdDHmFneO4NccVfdoOTX5NHJZq2oBbfyqTDv746', 'Genesis', 'Sanggalang', 'Aim High LTD INC', '', '', '', '', '', '', '', '', '#146 Tambubong-Batia Road, Tambubong, Bocaue, Bulacan', 'Bocaue', '3018', '+639972736678', 'Computers and Laptops', 'Aim High LTD INC', '', 'Chinabank Corporation', 1, 1, 1, 1, '1', 0, 0, 0, 0),
(19, 'ChamShop.png', 'Individual', 'sanggalanggv@gmail.com', 'Legendary Store', '+639972736678', 'f_ruit', '$2y$10$v8fpbcts4Zg/nm/ne.LN8.d1nu0oF9efdAv2xGjxFbPyIzd2N4Sr2', 'Huni', 'Chameleon', 'Chamshop Online Website', '124891258912', '', '', 'male', 'married', 'College Graduate', 'Web Developer', '', '#146 Tambubong-Batia Road, Tambubong, Bocaue, Bulacan', 'Bocaue', '3018', '09972736678', 'Computers and Laptops', 'Genesis V. Sanggalang', '12410251902519052', 'Chinabank Corporation', 1, 1, 1, 1, 'pending', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sellers_categories`
--

CREATE TABLE `sellers_categories` (
  `pk` int(11) NOT NULL,
  `username` text NOT NULL,
  `category` text NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sellers_categories`
--

INSERT INTO `sellers_categories` (`pk`, `username`, `category`, `archived`) VALUES
(1, 'aimhighltd', 'Laundry and Cleaning', 1),
(2, 'aimhighltd', 'Laundry and Cleaning', 1),
(3, 'aimhighltd', 'Bags and Travel', 1),
(4, 'aimhighltd', 'TV, Audio / Video, Gaming and Wearables', 1),
(5, 'aimhighltd', 'Stationery and Craft', 0),
(6, 'aimhighltd', 'Bags and Travel', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sellers_documents`
--

CREATE TABLE `sellers_documents` (
  `pk` int(11) NOT NULL,
  `username` text NOT NULL,
  `first_doc` text NOT NULL,
  `second_doc` text NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sellers_documents`
--

INSERT INTO `sellers_documents` (`pk`, `username`, `first_doc`, `second_doc`, `archived`) VALUES
(1, 'f_ruit', 'FbEr0dp5thudIkagszw0q8Vq4KvwBHC2hpnEt5TpI2kol4B1Es.jpg', 'uRPEVOzYcEFRrVt1NFiKcmVwTUYkGiYxRkd6eZj5TUJvQR29Pg.jpg', 0),
(2, 'f_ruit', 'ReQnbSFvofEC5r6hnewt9QUych4FQG2MrDKX5NQzhGJEq9SSLX.jpg', 'pYp5Bynhr6vXTh9a9xDCRkqmEz9l5nfYcJmbXSmkcyFMldS9wW.jpg', 0),
(3, 'f_ruit', 'ReQnbSFvofEC5r6hnewt9QUych4FQG2MrDKX5NQzhGJEq9SSLX.jpg', 'pYp5Bynhr6vXTh9a9xDCRkqmEz9l5nfYcJmbXSmkcyFMldS9wW.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sellers_followers`
--

CREATE TABLE `sellers_followers` (
  `pk` int(11) NOT NULL,
  `username` text NOT NULL,
  `sellers_pk` int(11) NOT NULL,
  `date_followed` date NOT NULL,
  `time_followed` time NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sellers_followers`
--

INSERT INTO `sellers_followers` (`pk`, `username`, `sellers_pk`, `date_followed`, `time_followed`, `archived`) VALUES
(39, 'aimhighltd', 18, '2018-01-22', '14:37:49', 1),
(40, 'aimhighltd', 18, '2018-01-22', '14:39:22', 1),
(41, 'aimhighltd', 18, '2018-01-22', '14:40:31', 0),
(42, 'sanggalanggv', 18, '2018-01-24', '15:42:51', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sellers_products`
--

CREATE TABLE `sellers_products` (
  `pk` int(11) NOT NULL,
  `product_name` text NOT NULL,
  `product_description` text NOT NULL,
  `product_category` text NOT NULL,
  `product_picture` text NOT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `uploaded_by` text NOT NULL,
  `wishlist` int(11) NOT NULL,
  `recommended` int(11) NOT NULL,
  `approved` text NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sellers_products`
--

INSERT INTO `sellers_products` (`pk`, `product_name`, `product_description`, `product_category`, `product_picture`, `price`, `stock`, `uploaded_by`, `wishlist`, `recommended`, `approved`, `archived`) VALUES
(17, 'Penguin & Jellyfish', 'You can eat them with ease.                            ', 'Groceries', 'S6XbJ11lM4Lvd9VRrslU4TEEsjYj5F8vvn1DIj0WuKF9visCpl.jpg', 500, 100, 'aimhighltd', 0, 0, '', 1),
(18, 'dopsp', 'adso                            ', 'Bags and Travel', 't1hhEf0vfMcvFGqOLWCJelW1pwjqZEORJ1ZtJFuGYedpVH1uPQ.jpg', 123, 123, 'aimhighltd', 0, 0, '', 1),
(19, 'x', 'x', 'Bags and Travel', 'qQ1JmZT2NFH7A7rok1dvWEbkX1qbC8J1rv8M007llGgjFdxd1y.jpg', 123, 123, 'aimhighltd', 0, 0, '', 1),
(20, 'z', 'z', 'Bags and Travel', 'LYFR58hluUepNrbl6Rdsne7w4Xt90l8cXLnKzg32RpKyP1f9x5.png', 123, 123, 'aimhighltd', 0, 0, '', 1),
(21, 'Accountabilty & Penguin & Jellyfish', 'You will held accountable in every things you\'ve done.\r\n', 'Health & Beauty', 'e49tMkvLRikwIWugdUjRLnlO7lS272CxZP76VH15Bf6oaJIftl.png', 800, 5, 'aimhighltd', 2, 2, 'yes', 0),
(22, 'Kappa', 'It is a joke, opposite of the said thing.', 'Health & Beauty', '1XPGzxQAszd0RqGwuutrci4YIe6sMswNNlLS4rV2ocMH22cgr0.jpg', 123, 123, 'aimhighltd', 1, 2, 'yes', 0),
(23, '123', '123', 'Bags and Travel', '3BESwfskbkGM68QATQ9b7v50Ec9STQHeZvFXEYeTfFZLMylkXG.jpg', 213, 123, 'aimhighltd', 1, 0, 'pending', 0),
(24, 'zxcz', 'asdasdasda\r\nasdasdasda\r\nzxcczxczxcz\r\nasdadqweqweq', 'Bags and Travel', 'KBGqQBQIee67RW3QcLFqFiPa2ZArL4X0Ne0GuGKpqBAnDbElfJ.png', 123, 123, 'aimhighltd', 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sellers_product_pictures`
--

CREATE TABLE `sellers_product_pictures` (
  `pk` int(11) NOT NULL,
  `product_name` text NOT NULL,
  `product_picture` text NOT NULL,
  `uploaded_by` text NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sellers_product_pictures`
--

INSERT INTO `sellers_product_pictures` (`pk`, `product_name`, `product_picture`, `uploaded_by`, `archived`) VALUES
(91, 'Penguin & Jellyfish', 'bup7bOzXRCRyuF4Bb4f7Cx76lJ4ASdnyIZ0iT4oTBz3LfLBIeo.jpg', 'aimhighltd', 0),
(92, 'Penguin & Jellyfish', 'VkqOhtSGUPbMPgNCXF97olvYa67ymsrfx5Pk35ODdXFJutEUe0.jpg', 'aimhighltd', 0),
(93, 'Penguin ', 'mIAZKSzyszXjJmYkF68MpjBsxM9TpCdFiFmQVa56a4Uqt3BMKJ.jpg', 'aimhighltd', 0),
(94, 'Penguin ', 'S6XbJ11lM4Lvd9VRrslU4TEEsjYj5F8vvn1DIj0WuKF9visCpl.jpg', 'aimhighltd', 0),
(95, 'dopsp', 'hPcaJnpJ5J3lOPGwZFyYYWGStAggWoo9AFzuerCoodcVRO7Eph.jpg', 'aimhighltd', 0),
(96, 'dopsp', 'aNhE1KV2j7MxY2KhF5WS5uzM3nbzv1JTrBcCIOzINX03dNdiz8.jpg', 'aimhighltd', 0),
(97, 'dopsp', 'QLhaF0Wyr5Hx0iOc55zuElTvrMIX0KrigKpwiZob9N6NZmvfiJ.jpg', 'aimhighltd', 0),
(98, 'dopsp', 'F8gUFoYaSSMO1FnhkvxSL4buh2c7ybe4A0dGct1NkwWpPiRFFr.jpg', 'aimhighltd', 0),
(99, 'dopsp', '64hfvMQtHEJBFcGmKTxDYv2lkRRafTjNKau5Pn3U1S7rycE9TE.jpg', 'aimhighltd', 0),
(100, 'dopsp', 'SUpEWLGeLfCWNvPFyDltAqszHPGRBhZ5MCTXjihpS4pFPD46TI.jpg', 'aimhighltd', 0),
(101, 'dopsp', 'ABTsYGgwqV5qNBhXrqLJRqeBYtlWIBdClDvmqyVp8ugS2QBgMS.jpg', 'aimhighltd', 0),
(102, 'dopsp', 't1hhEf0vfMcvFGqOLWCJelW1pwjqZEORJ1ZtJFuGYedpVH1uPQ.jpg', 'aimhighltd', 0),
(103, 'x', 'J1VKCANGNIU2FlShwmSMPah9EJhsFjQC3Ke8H6Xs3kNRz1A6sZ.jpg', 'aimhighltd', 0),
(104, 'x', '3K2lrZuM0k49GosYb3yv1kIdXpbQgazmVXTipcwb9B86sqnDio.png', 'aimhighltd', 0),
(105, 'x', 'YDolb3UOfbqyAFcl7AtZPLLx8SMnr55SrwuBo7xxXdPHWeuDbr.png', 'aimhighltd', 0),
(106, 'x', 'mZ8OXgGuyXcI99zN8apC2D6ozDo4nIegNIMeadCXXMGAmw3434.jpg', 'aimhighltd', 0),
(107, 'x', 'ru19tsZRZ52fJM7nNu1YushvS8QNY1hy6Jhut8Vbtmv44WMKUc.jpg', 'aimhighltd', 0),
(108, 'x', 'hkNTJPM5p5avCzdzIUHqJG1gxS2pHtFbSybWNXhMtZiLZ3yOo7.jpg', 'aimhighltd', 0),
(109, 'x', 'qQ1JmZT2NFH7A7rok1dvWEbkX1qbC8J1rv8M007llGgjFdxd1y.jpg', 'aimhighltd', 1),
(110, 'z', 'JAJ0E0DjfYkm6lIyqCktcg9LmTRhXK7xCAefOam3hBXzQpcKZA.png', 'aimhighltd', 0),
(111, 'z', 'ob7bSln3tpem1EpxwW0m1wH7UqKcyC3YLpKHyKNaYiWwHeBnVR.png', 'aimhighltd', 0),
(112, 'z', 'EM1CgFTLXVkzJvJJJcVW1hZPDb6F8aEuYY5TZZchzhfxXT7hDd.jpg', 'aimhighltd', 0),
(113, 'z', 'yXQ0i6MNcMj8VvrC1bmSfufqXTQiemH16eg4HvYbL9hKO1IngX.png', 'aimhighltd', 0),
(114, 'z', 'LYFR58hluUepNrbl6Rdsne7w4Xt90l8cXLnKzg32RpKyP1f9x5.png', 'aimhighltd', 0),
(115, 'Accountabilty', 'WJH8BEqGdPleXYARHPeOttKURlvF8MCY0iApCKv5MA9vSz4suN.png', 'aimhighltd', 0),
(116, 'Accountabilty', 'e49tMkvLRikwIWugdUjRLnlO7lS272CxZP76VH15Bf6oaJIftl.png', 'aimhighltd', 0),
(117, 'Kappa', '1XPGzxQAszd0RqGwuutrci4YIe6sMswNNlLS4rV2ocMH22cgr0.jpg', 'aimhighltd', 0),
(118, 'Accountabilty', 'nNkFddwuxlBL3WkRBoYrRtqSvH3sMg8fG6jpvORr1HRXQFPnkU.jpg', 'aimhighltd', 0),
(119, 'Accountabilty', 'GlW1fbwUuey7h17n0Y1Z9RAsvrq7truK9JBDXr61Cmtg9dKZY2.png', 'aimhighltd', 0),
(120, 'Accountabilty', 'rfwNgXBcXmpcvlc3z6gIU3KxoyDvcSQ5koZHQB2AdGEmvixjwE.png', 'aimhighltd', 0),
(121, 'Accountabilty', 'WggKDUYPE7yurbXmi52uonlezWmpbRn6ULJoC4R1b9KItmZUGI.jpg', 'aimhighltd', 0),
(122, 'Accountabilty', 'TZazvRcX6T3OJrUgivhZsTKJAazSE3LTsgt8tOkAMjhyK7uzoX.jpg', 'aimhighltd', 0),
(123, 'Accountabilty', 'bvRtamAATppqOaNNRjDALKPLEnyZgCLcPSQFnMXSPQCqUEMCEB.jpg', 'aimhighltd', 0),
(124, 'Accountabilty', 'ZGjzlmxtU3JQWx7ADZChJHftmHjFtzKKT6AkOkO90CATi1rEY2.jpg', 'aimhighltd', 1),
(125, 'Accountabilty', '7dtY1l1ApPjl6ngymHfJ2MeCTetRQBNyJhRa1YZYMfBQrgYt7B.jpg', 'aimhighltd', 0),
(126, '123', 'T9cn8IPE22fDz2ThoTqZwFvpQ3zxi5zHqPSwwiIHwFpn7FOWfb.jpg', 'aimhighltd', 0),
(127, '123', '3BESwfskbkGM68QATQ9b7v50Ec9STQHeZvFXEYeTfFZLMylkXG.jpg', 'aimhighltd', 0),
(128, 'zxcz', 'KBGqQBQIee67RW3QcLFqFiPa2ZArL4X0Ne0GuGKpqBAnDbElfJ.png', 'aimhighltd', 0),
(129, 'Accountabilty', 'ImmcJ9Z9VpVY36miFJsYevoENgXzlJhlrxLxoBw5aqBdStIEdR.jpg', 'aimhighltd', 0),
(130, 'Accountabilty', 'qy8fe9ur7FQKcSl0ogKivOoBAX8x7mxt3V9JLZ9CB7O1Vs0q5t.png', 'aimhighltd', 1),
(131, 'Accountabilty', 'bLogG5lHtSqsBTNa22Kwm6vDP8k4JNsgJ0QybFWwdvGpK3gQAZ.png', 'aimhighltd', 1),
(132, 'Accountabilty', 'T1xzoTlMxf0TNtl4HtoxE0ShRHbEbNZ4y3B4swvy3c2UYuHtI0.jpg', 'aimhighltd', 1),
(133, 'Accountabilty', 'yxyNc7pASNMd9XQUOgz9Guv7YOQcDYaS7nq23pPubURPJbqYBY.jpg', 'aimhighltd', 1),
(134, 'Accountabilty', 'n4zWmBYRKIiYJEm9cjrIPo9E7FNSq3CC7OtEnQT0ISmT5xL5Ok.jpg', 'aimhighltd', 1),
(135, 'Accountabilty', 'yvlCmkm3de1Fll2O53UmRNb5gxPkNysFlXdFjbVOobwYkjdzx8.jpg', 'aimhighltd', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sellers_recommendation`
--

CREATE TABLE `sellers_recommendation` (
  `pk` int(11) NOT NULL,
  `username` text NOT NULL,
  `sellers_pk` int(11) NOT NULL,
  `date_recommended` date NOT NULL,
  `time_recommended` time NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sellers_recommendation`
--

INSERT INTO `sellers_recommendation` (`pk`, `username`, `sellers_pk`, `date_recommended`, `time_recommended`, `archived`) VALUES
(1, 'aimhighltd', 18, '2018-01-21', '14:46:15', 1),
(2, 'aimhighltd', 18, '2018-01-22', '14:40:11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_address`
--

CREATE TABLE `shipping_address` (
  `pk` int(11) NOT NULL,
  `full_name` text NOT NULL,
  `address` text NOT NULL,
  `province` text NOT NULL,
  `municipality` text NOT NULL,
  `city` text NOT NULL,
  `mobile` text NOT NULL,
  `account_pk` int(11) NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shipping_address`
--

INSERT INTO `shipping_address` (`pk`, `full_name`, `address`, `province`, `municipality`, `city`, `mobile`, `account_pk`, `archived`) VALUES
(20, 'Glenda Sanggalang', '#146 Tambubong - Batia Road', '#146 Tambubong - Batia Road', 'Bocaue', 'Tambubong', '09972736778', 0, 0),
(21, 'Rose Ann F. Del Rosario', 'J. Garcia St.', 'J. Garcia St.', 'Plaridel', 'Banga 2nd', '09972736778', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `shopping_cart`
--

CREATE TABLE `shopping_cart` (
  `pk` int(11) NOT NULL,
  `buyer_pk` text NOT NULL,
  `seller` text NOT NULL,
  `product_pk` int(11) NOT NULL,
  `amount_ordered` int(11) NOT NULL,
  `date_placed` date NOT NULL,
  `time_placed` time NOT NULL,
  `hash` text NOT NULL,
  `approved` text NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopping_cart`
--

INSERT INTO `shopping_cart` (`pk`, `buyer_pk`, `seller`, `product_pk`, `amount_ordered`, `date_placed`, `time_placed`, `hash`, `approved`, `archived`) VALUES
(171, 'e17cb56036d31e7d4c87eed66ae48024', 'aimhighltd', 21, 5, '2018-01-25', '22:24:05', 'd429c59a6c23d0db9a38b5fc3f38941c', 'yes', 1),
(172, 'e17cb56036d31e7d4c87eed66ae48024', 'aimhighltd', 22, 5, '2018-01-25', '22:24:05', 'd429c59a6c23d0db9a38b5fc3f38941c', 'yes', 1),
(173, 'e17cb56036d31e7d4c87eed66ae48024', 'aimhighltd', 23, 5, '2018-01-25', '22:24:05', 'd429c59a6c23d0db9a38b5fc3f38941c', 'yes', 1),
(174, 'e17cb56036d31e7d4c87eed66ae48024', '', 21, 1, '2018-01-26', '15:03:03', '6196442a05eb918c20874a27d09ed6e4', 'pending', 1),
(175, 'e17cb56036d31e7d4c87eed66ae48024', 'aimhighltd', 22, 1, '2018-01-26', '15:03:03', '6196442a05eb918c20874a27d09ed6e4', 'pending', 1),
(176, 'e17cb56036d31e7d4c87eed66ae48024', 'aimhighltd', 21, 1, '2018-01-26', '15:03:03', '6196442a05eb918c20874a27d09ed6e4', 'pending', 1),
(177, 'e17cb56036d31e7d4c87eed66ae48024', 'aimhighltd', 23, 1, '2018-01-26', '15:46:14', 'e77182a48dc5692e6de1270260407945', 'pending', 1),
(178, 'e17cb56036d31e7d4c87eed66ae48024', 'aimhighltd', 21, 1, '2018-01-26', '15:47:24', '81560da5a458f26399ca2394f75e2077', 'pending', 1),
(179, 'e17cb56036d31e7d4c87eed66ae48024', 'aimhighltd', 21, 1, '2018-01-26', '15:47:24', '81560da5a458f26399ca2394f75e2077', 'pending', 1),
(180, 'e17cb56036d31e7d4c87eed66ae48024', 'aimhighltd', 21, 1, '2018-01-26', '15:48:08', 'd2106fad2f7db8bd591141cf2f53ef95', 'pending', 1),
(181, 'e17cb56036d31e7d4c87eed66ae48024', 'aimhighltd', 21, 3, '2018-01-27', '13:52:39', '0b044b88419f4ecd26a23ed6ac31e503', 'yes', 1),
(182, 'e17cb56036d31e7d4c87eed66ae48024', 'aimhighltd', 21, 1, '2018-01-27', '14:49:43', 'a56a238ba5eb60372f11c9fd8a75535c', 'yes', 1),
(183, 'e17cb56036d31e7d4c87eed66ae48024', 'aimhighltd', 22, 1, '2018-01-27', '14:49:43', 'a56a238ba5eb60372f11c9fd8a75535c', 'yes', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `pk` int(15) NOT NULL,
  `default_picture` text NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `email` text NOT NULL,
  `mobile` text NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `seller` tinyint(1) NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`pk`, `default_picture`, `first_name`, `last_name`, `email`, `mobile`, `username`, `password`, `seller`, `archived`) VALUES
(8, 'ChamShop.png', 'Genesis', 'Sanggalang', 'sanggalanggv@gmail.com', '09972736678', 'sanggalanggv', '$2y$10$tvLc5JeVlS4aMAdtz8v5POPymGpMGFQekyvraVzZJgtteQtfyausq', 0, 0),
(9, 'ChamShop.png', 'Genesis', 'Sanggalang', 'dota2nigen@gmail.com', '09972736678', 'aimhighltd', '$2y$10$b8hwgR359EKyoiyMmGvdXuJhH.VcZXApAKXpdWFXyLSGOST7TyH6u', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_wishlist`
--

CREATE TABLE `user_wishlist` (
  `pk` int(11) NOT NULL,
  `username` text NOT NULL,
  `product_pk` int(11) NOT NULL,
  `date_listed` date NOT NULL,
  `time_listed` time NOT NULL,
  `archived` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_wishlist`
--

INSERT INTO `user_wishlist` (`pk`, `username`, `product_pk`, `date_listed`, `time_listed`, `archived`) VALUES
(1, 'sanggalanggv', 21, '2018-01-19', '20:20:48', 1),
(2, 'sanggalanggv', 21, '2018-01-19', '20:45:56', 1),
(3, 'sanggalanggv', 21, '2018-01-20', '12:40:57', 0),
(4, 'aimhighltd', 21, '2018-01-21', '12:44:16', 1),
(5, 'aimhighltd', 21, '2018-01-21', '13:41:59', 1),
(6, 'aimhighltd', 22, '2018-01-21', '13:51:50', 1),
(7, 'aimhighltd', 22, '2018-01-21', '13:51:57', 1),
(8, 'aimhighltd', 22, '2018-01-21', '13:52:22', 1),
(9, 'sanggalanggv', 23, '2018-01-22', '12:22:06', 0),
(10, 'sanggalanggv', 22, '2018-01-22', '12:22:18', 0),
(11, 'aimhighltd', 22, '2018-01-22', '14:15:11', 1),
(12, 'aimhighltd', 22, '2018-01-22', '14:15:45', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `billing_address`
--
ALTER TABLE `billing_address`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `product_comments`
--
ALTER TABLE `product_comments`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `product_recommendation`
--
ALTER TABLE `product_recommendation`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `sellers`
--
ALTER TABLE `sellers`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `sellers_categories`
--
ALTER TABLE `sellers_categories`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `sellers_documents`
--
ALTER TABLE `sellers_documents`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `sellers_followers`
--
ALTER TABLE `sellers_followers`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `sellers_products`
--
ALTER TABLE `sellers_products`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `sellers_product_pictures`
--
ALTER TABLE `sellers_product_pictures`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `sellers_recommendation`
--
ALTER TABLE `sellers_recommendation`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `shipping_address`
--
ALTER TABLE `shipping_address`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `shopping_cart`
--
ALTER TABLE `shopping_cart`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`pk`);

--
-- Indexes for table `user_wishlist`
--
ALTER TABLE `user_wishlist`
  ADD PRIMARY KEY (`pk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `billing_address`
--
ALTER TABLE `billing_address`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `product_comments`
--
ALTER TABLE `product_comments`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `product_recommendation`
--
ALTER TABLE `product_recommendation`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `sellers`
--
ALTER TABLE `sellers`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `sellers_categories`
--
ALTER TABLE `sellers_categories`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sellers_documents`
--
ALTER TABLE `sellers_documents`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sellers_followers`
--
ALTER TABLE `sellers_followers`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `sellers_products`
--
ALTER TABLE `sellers_products`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `sellers_product_pictures`
--
ALTER TABLE `sellers_product_pictures`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `sellers_recommendation`
--
ALTER TABLE `sellers_recommendation`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `shipping_address`
--
ALTER TABLE `shipping_address`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `shopping_cart`
--
ALTER TABLE `shopping_cart`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `pk` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user_wishlist`
--
ALTER TABLE `user_wishlist`
  MODIFY `pk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
