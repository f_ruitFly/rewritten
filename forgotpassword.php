<?php
//IF INDIVIDUAL SELLER
echo
'
<!DOCTYPE html>
<html>
<head>
    <title>Chamshop | Adapting to your needs</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- BOOTSTRAP -->
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="ASSETS/BOOTSTRAP/CSS/bootstrap-3.3.7.min.css">
    <link rel="stylesheet" type="text/css" href="ASSETS/DIST/font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- FONTS-->
    
    <!-- JS -->
    <script src="ASSETS/JQUERY/jquery-3.2.1.js"></script>
    <script src="ASSETS/BOOTSTRAP/JS/bootstrap-3.3.7.min.js"></script>

    <!-- CUSTOM JS -->
    
    <!-- CUSTOM DESIGNS -->
    <link rel="stylesheet" type="text/css" href="ASSETS/CSS/navbar.css">
    <link rel="stylesheet" type="text/css" href="ASSETS/CSS/profile.css">
    <link rel="stylesheet" type="text/css" href="ASSETS/CSS/frontend.css">
</head>
<script src="JS/form-validation.js"></script>
<div class="container" style="margin-top: 50px">
	<div class="row">
		<div style="border: solid 1px #AAAAAA; background-color: #dddddd;" class="col-md-8 col-md-offset-2">
			<div class="text-center">
				<img class="img-rounded" style="width: 180px; height:auto" src="ASSETS/IMAGE/chamee.png">
			</div>
			<div class="col-md-12 text-center">
				<h2 class="modal-title">Forgot Password</h2>
			</div>
			<div class="col-md-12" style="margin-top:2%">
				<h3 class="modal-title">Welcome User!</h3>
			</div>
			<div class="col-md-12">
				<h5 class="modal-title">I`m <span style="font-weight:900">Huni!</span> Your friendly chameleon.</h5>
			</div>
			<div class="col-md-12">
				<h5 class="modal-title">Did you forgot your password? Don\'t worry, we will send you a link in your registered e-mail and follow the steps there in order to change your password in a new one. Thank you!</h5>
			</div>
			<div class="col-md-12">
				<form class="text-center" autocomplete="off" style="margin-top:2%" action="FUNCTIONS/ForgotPassword.php" method="post">
					<div>
						<label for="txtMobileAuth">Are you a seller or buyer?</label>
					</div>
					<div class="form-group">
						<input id="seller"type="radio" name="rdbType" value="Seller"> Seller
						<input id="buyer"type="radio" name="rdbType" value="Buyer"> Buyer
					</div>
					<div>
						<label for="txtMobileAuth">Please enter your registered e-mail address.</label>
					</div>
					<div class="form-group">
						<input type="text" style="height:50px;" id="forgotemail" name="txtEmail" placeholder="Your e-mail" required size="50"><br>
						<span id="status_email"></status>
					</div>
					<div style="margin-top: 1%">
						<button style="width: 25%" type="submit" class="btn btn-success btn-md" id="register" name="emailconfirm"><span class="fa fa-check-circle"></span> Continue</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
';
