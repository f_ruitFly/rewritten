<?php
include('header.php');
include('FUNCTIONS/fCartContents.php');
if ($cart_total > 0){
	if (isset($_SESSION['pk'])){
		include_once('FUNCTIONS/fGetAddressBook.php');
	echo
	'
	<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<a href="addressbook?u='.$_SESSION['username'].'"><span style="font-weight:600">Manage your address book here!</span></a>
					</div>
					<form action="FUNCTIONS/fPlaceOrder.php" method="post">
						<div class="col-md-4" style="margin-top:10px">
							<div style="margin-bottom: 10px">
								<span style="font-weight:600">Select a Shipping Address</span>
							</div>
								';
								while ($row = mysqli_fetch_array($getshipping)){
								echo
								'
									<div class="col-md-12" style="background-color: #FFFFFF; margin-bottom: 10px";">
										<div>
											<span style="margin-left: 40px">'.$row['full_name'].'</span>
										</div>
										<div>
										<input type="radio" name="rdbShipping" value="'.$row['pk'].'">
											<span style="margin-left: 23px">'.$row['address'].', '.$row['city'].'</span>
										</div>
										<div>
											<span style="margin-left: 40px">'.$row['municipality'].', '.$row['province'].'</span>
										</div>
										<div>
											<span style="margin-left: 40px">'.$row['mobile'].'</span>
										</div>
									</div>
								';}
								echo
								'
						</div>
						<div class="col-md-4" style="margin-top:10px">
							<div style="margin-bottom: 10px">
								<span style="font-weight:600">Select a Billing Address</span>
							</div>
								';
								while ($row = mysqli_fetch_array($getbilling)){
								echo
								'
									<div class="col-md-12" style="background-color: #FFFFFF; margin-bottom: 10px";">
										<div>
											<span style="margin-left: 40px">'.$row['full_name'].'</span>
										</div>
										<div>
										<input type="radio" name="rdbBilling" value="'.$row['pk'].'">
											<span style="margin-left: 23px">'.$row['address'].', '.$row['city'].'</span>
										</div>
										<div>
											<span style="margin-left: 40px">'.$row['municipality'].', '.$row['province'].'</span>
										</div>
										<div>
											<span style="margin-left: 40px">'.$row['mobile'].'</span>
										</div>
									</div>
								';}
								echo
								'
						</div>
						<div class="col-md-4" style="margin-top:10px">
							<div style="margin-bottom: 10px">
								<span style="font-weight:600">Order Summary</span>
							</div>
							<table class="table" style="background-color:#FFFFFF">
								<thead>
									<tr class="info">
										<th>Product</th>
								        <th>Quantity</th>
								        <th>Price</th>
							        </tr>
								</thead>
								<tbody>
									';
									$total = 0;
									
									while ($row = mysqli_fetch_array($getcontents)){
										$pk = $row['product_pk'];
										$getproducts = mysqli_query($connect, "SELECT * FROM sellers_products WHERE pk = $pk");
										while ($products = mysqli_fetch_array($getproducts)){
											$total += $row['amount_ordered'] * $products['price'];
										echo
										'
										<tr>
											<td class="col-md-4">'.$products['product_name'].'</td>
											<td>'.$row['amount_ordered'].'</td>
											';
											if ($products['sale'] == false){
											$total += $row['amount_ordered'] * $products['price'];
											echo
											'
											<td>₱ '.$row['amount_ordered'] * $products['price'].'</td>
											';
											}else{
											$total += $row['amount_ordered'] * $products['new_price'];
											echo
											'
											<td>₱ '.$row['amount_ordered'] * $products['new_price'].'</td>
											';
											}
											echo
											'
										</tr>
										';
									}}
									echo
									'
										<tr class="success">
										<td class="col-md-4" style="font-weight:600">Total</td>
											<td></td>
											<td style="color: orange; font-weight:600">₱ '.$total.'</td>
										</tr>
								</tbody>
							</table>
							<div class="col-md-12">
								<button class="shrink" style="height: 50px; width: inherit; background-color: #6FBC92; border-radius; 0px; font-weight:600; color:white;" type="submit" name="loggedorder">Place Order
								</button>
							</div>	
						</div>	
					</form>
				</div>
			<div>
		</div> 
	';
	}else{
	echo
	'
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12">
						<span class="page-span-title">Addressbook</span>
					</div>
					<div class="col-md-4">
						<div class="col-md-12 div-margin-top">
							<span class="address-label">Shipping Address</span>
						</div>
						<div class="col-md-12 div-margin-top">
							<form autocomplete="off" action="FUNCTIONS/fPlaceOrder.php" method="post">
								<div class="form-group">
									<label class="span-label">Full Name</label>
									<input class="form-control" type="text" name="txtShippingFullName" required>
								</div>
								<div class="form-group">
									<label class="span-label">Complete Address</label>
									<input class="form-control" type="text" name="txtShippingAddress" required>
								</div>
								<div class="form-group">
									<label class="span-label">Province</label>
									<input class="form-control" type="text" name="txtShippingProvince" required>
								</div>
								<div class="form-group">
									<label class="span-label">Municipality</label>
									<input class="form-control" type="text" name="txtShippingMunicipality" required>
								</div>
								<div class="form-group">
									<label class="span-label">City</label>
									<input class="form-control" type="text" name="txtShippingCity" required>
								</div>
								<div class="form-group">
									<label class="span-label">Mobile</label>
									<input class="form-control" type="text" name="txtShippingMobile" required>
								</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="col-md-12 div-margin-top">
							<span class="address-label">Billing Address</span>
						</div>
						<div class="col-md-12 div-margin-top">
								<div class="form-group">
									<label class="span-label">Full Name</label>
									<input class="form-control" type="text" name="txtBillingFullName" required>
								</div>
								<div class="form-group">
									<label class="span-label">Complete Address</label>
									<input class="form-control" type="text" name="txtBillingAddress" required>
								</div>
								<div class="form-group">
									<label class="span-label">Province</label>
									<input class="form-control" type="text" name="txtBillingProvince" required>
								</div>
								<div class="form-group">
									<label class="span-label">Municipality</label>
									<input class="form-control" type="text" name="txtBillingMunicipality" required>
								</div>
								<div class="form-group">
									<label class="span-label">City</label>
									<input class="form-control" type="text" name="txtBillingCity" required>
								</div>
								<div class="form-group">
									<label class="span-label">Mobile</label>
									<input class="form-control" type="text" name="txtBillingMobile" required>
								</div>
								<div class="form-group">
									<label class="span-label">Email</label>
									<input class="form-control" type="email" name="txtEmail" required>
								</div>
						</div>
					</div>
					<div class="col-md-4" style="margin-top:10px">
							<div style="margin-bottom: 10px">
								<span style="font-weight:600">Order Summary</span>
							</div>
							<table class="table" style="background-color:#FFFFFF">
								<thead>
									<tr class="info">
										<th>Product</th>
								        <th>Quantity</th>
								        <th>Price</th>	
							        </tr>
								</thead>
								<tbody>
									';
									$total = 0;
									
									while ($row = mysqli_fetch_array($getcontents)){
										$pk = $row['product_pk'];
										$getproducts = mysqli_query($connect, "SELECT * FROM sellers_products WHERE pk = $pk");
										while ($products = mysqli_fetch_array($getproducts)){

										echo
										'
										<tr>
											<td class="col-md-4">'.$products['product_name'].'</td>
											<td>'.$row['amount_ordered'].'</td>';
											if ($products['sale'] == false){
											$total += $row['amount_ordered'] * $products['price'];
											echo
											'
											<td>₱ '.$row['amount_ordered'] * $products['price'].'</td>
											';
											}else{
											$total += $row['amount_ordered'] * $products['new_price'];
											echo
											'
											<td>₱ '.$row['amount_ordered'] * $products['new_price'].'</td>
											';
											}
											echo
											'
											
										</tr>
										';
									}}
									echo
									'
										<tr class="success">
										<td class="col-md-4" style="font-weight:600">Total</td>
											<td></td>
											<td style="color: orange; font-weight:600">₱ '.$total.'</td>
										</tr>
								</tbody>
							</table>
							<div class="col-md-12">
								<button class="shrink" style="height: 50px; width: inherit; background-color: #6FBC92; border-radius; 0px; font-weight:600; color:white;" type="submit" name="notloggedorder">Place Order
								</button>
							</div>	
						</form>
				</div>
			</div>
		</div>
	';
	}
}else{
echo
'
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 text-center">
				<span>You have no items in your cart, please add one.</span>
			</div>
		</div>
	</div>
';
}
