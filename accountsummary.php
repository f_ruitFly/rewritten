<?php
include("header.php");
if (!(isset($_SESSION['pk']))){
header('location: login');
}else{
include ("FUNCTIONS/fProfile.php");

echo
'
<script src="JS/profile-validation.js"></script>
<div class="container-fluid">
	<nav class="col-md-2">
		<ul class="nav nav-pills nav-stacked">
			<li>
				<span style="font-weight: 600; font-size: 18px" class="fa fa-user-circle-o"></span>
				<span style="font-weight: 600; font-size: 18px">'.$first_name.' '.$last_name.'</span>
			</li>
			<li><a href="personalinformation?u='.$_SESSION['username'].'" id="sideNavFont">• Personal Information <span class="fa fa-info"></span></a></li>
			<li><a href="wishlist?u='.$_SESSION['username'].'" id="sideNavFont">• Wishlist <span class="fa fa-magic"></span></a></li>
			<li><a href="reviews?u='.$_SESSION['username'].'" id="sideNavFont">• My Reviews <span class="fa fa-pencil"></span></a></li>
			<li><a href="addressbook?u='.$_SESSION['username'].'" id="sideNavFont">• Address Book <span class="fa fa-address-book"></span></a></li>
			<li><a href="notificationcenter?u='.$_SESSION['username'].'" id="sideNavFont">• Notification Center <span class="fa fa-exclamation-triangle"></span></a></li>
			<li><a href="buyandsell?u='.$_SESSION['username'].'" id="sideNavFont" >• Sell your preloved items <span class="fa fa-shopping-basket"></span></a></li>
			<li><a id="sideNavFont" data-toggle="pill" href="#accountSummary">• Be a seller! <span class="fa fa-briefcase"></span></a></li>
		</ul>
	</nav>
<div class="col-md-10"style=" background-color: #ffffff">
	<div class="tab-content">
		<div id="accountSummary">
			<div class="col-md-12;">
				<span style="font-size: 20px; font-weight: 600;">Orders</span>
				<span class="pull-right" style="font-size: 16px; font-weight: 600;"><a>SEE MORE</a></span>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6 text-center" style="border: solid 1px #AAAAAA; background-color: #dddddd; padding:10px;">
							<span class="fa fa-check-circle" style="font-size:34px; font-weight:600; color:green;"></span><br>
							<span style="font-size:30px; font-weight:600;">0</span>
							<span style="font-size: 20px;">Successful <br> Orders</span>
						</div>
						<div class="col-md-6 text-center" style="border: solid 1px #AAAAAA; background-color: #dddddd; padding:10px;">
							<span class="fa fa-times-circle" style="font-size:34px; font-weight:600; color:red;"></span><br>
							<span style="font-size:30px; font-weight:600;">0</span>
							<span style="font-size: 20px;">Cancelled <br> Orders</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12" style="padding-top:10px">
			<span style="font-size: 20px; font-weight: 600;">Personal Information</span>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12" style="border: solid 1px #AAAAAA; background-color: #dddddd; padding:10px;">
							<div>
								<span style="font-size:16px;">'.$first_name.' '.$last_name.'</span>
							</div>

							<div>
								<span style="font-size: 16px;">'.$email.'</span>
							</div>
							<div>
								<span style="font-size: 16px;">'.$mobile.'</span>
							</div>
							<div style="padding-top:10px">
								<a style="cursor:pointer" data-toggle="modal" data-target="#updateEmail">CHANGE E-MAIL</a>
							</div>

							<div>
								<a style="cursor:pointer" data-toggle="modal" data-target="#updatePassword">CHANGE PASSWORD</a>
							</div>

							<div>
								<a style="cursor:pointer" class="pull-right" href="personalinformation?u='.$_SESSION['username'].'">EDIT</a>					
							</div>
					</div>
				</div>
			</div>

			<div class="col-md-6" style="padding-top:10px">
				<span class="pull-left" style="font-size: 20px; font-weight: 600;">Recent Transaction</span>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-12">
							<table class="table">
							    <thead>
							      <tr>
							        <th>Order number</th>
							        <th>Placed on</th>
							        <th>Total</th>
							        <th>Status</th>
							      </tr>
							    </thead>
							    <tbody>
							      <tr>
							        <td><a>0249124</a></td>
							        <td>MM/DD/YYYY</td>
							        <td>PHP</td>
							        <td>Delivered</td>
							      </tr>
							    </tbody>
						  </table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6" style="padding-top:10px">
				<span class="pull-left" style="font-size: 20px; font-weight: 600;">Wishlist, Reviews, & Recommendations </span>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-12">
							<div>
								<span><a href="wishlist?u='.$_SESSION['username'].'">'.$wishlist.'</a> items in wishlist.</span>
							</div>
							<div>
								<span><a href="reviews?u='.$_SESSION['username'].'">'.$comments.'</a> items reviewed.</span>
							</div>
							<div>
								<span><a href="reviews?u='.$_SESSION['username'].'">'.$recommendation.'</a> items in recommendation.</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

';
}


// UPDATE EMAIL
echo
'
 <div class="modal fade" id="updateEmail" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-title">
                	<div class="text-center">
						<img class="img-rounded" style="width: 180px; height:auto" src="ASSETS/IMAGE/chamee.png">
	            		<h3 style="font-weight:600;">Update Information</h3>
            		</div>
	            		<h5 style="padding-top:5px;">I`m <span style="font-weight:800">Huni! </span>Your friendly chameleon.</h5>
	            		<h5>Please do not leave the the fields blank. Thank you!</h5>
                </div>
            </div>
            <div class="modal-body">
	            <form autocomplete="off" style="margin-top:%" action="FUNCTIONS/fProfile.php?u='.$_SESSION['username'].'" method="post">
	            <div class="form-group">
					<span>Your current e-mail address: </span>
						<div>
							<label>'.$email.'</label>
						</div>
				</div>
	            <div class="form-group">
					<span class="fa fa-envelope"></span> <label for="txtEmail">E-mail *</label>
						<input style="width:55%" type="email" class="form-control" name="txtEmail" id="txtEmail" placeholder="example@email.com" required>
						<div>
							<span id="status_email"></span>
						</div>
				</div>
						<div class="btn-group">
							<div class="btn-group">
								<button id="register" class="btn btn-success" name="saveEmail" type="submit">Update </button>
							</div>
							<div class="btn-group">
								<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel </button>
							</div>
						</div>
	            </form>
            </div>
        </div>
    </div>
</div>
';

// CHANGE PASSWORD MODAL
echo
'
 <div class="modal fade" id="updatePassword" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-title">
                	<div class="text-center">
						<img class="img-rounded" style="width: 180px; height:auto" src="ASSETS/IMAGE/chamee.png">
	            		<h3 style="font-weight:600;">Update Information</h3>
            		</div>
	            		<h5 style="padding-top:5px;">I`m <span style="font-weight:800">Huni! </span>Your friendly chameleon.</h5>
	            		<h5>Please do not leave the the fields blank. Thank you!</h5>
                </div>
            </div>
            <div class="modal-body">
	            <form autocomplete="off" style="margin-top:%" action="FUNCTIONS/fProfile.php?u='.$_SESSION['username'].'" method="post">
	            <div class="form-group">
					<label for="txtCurrentPassword">Current Password: </label>
						<input type="password" class="form-control" name="txtCurrentPassword" id="txtCurrentPassword" placeholder="Your current password" required size="40">
						<span id="error_current_password"></span>
				</div>
				<div class="form-group">
					<label for="txtNewPassword">New Password: </label>
						<input type="password" class="form-control" name="txtNewPassword" id="txtNewPassword" placeholder="Your new password" required size="40">
						<span id="error_new_password"></span>
				</div>
				<div class="form-group">
					<label for="txtRetype">Re-type New Password: </label>
						<input type="password" class="form-control" name="txtRetypePassword" id="txtRetypePassword" placeholder="Re-type your new password" required size="40">
						<span id="error_retype_password"></span>
				</div>
						<div class="btn-group">
							<div class="btn-group">
								<button class="btn btn-success" name="savePassword" type="submit" id="savePassword">Update</button>
							</div>
							<div class="btn-group">
								<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel </button>
							</div>
						</div>
	            </form>
            </div>
        </div>
    </div>
</div>
';