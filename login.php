<?php
include ("header.php");
echo
'
<div class="container col-md-6 col-md-offset-3">
	<div class="row">
		<div style="border: solid 2px #6EBC91; background-color: #ffffff" class="col-md-10 col-md-offset-1">
			<div class="text-center">
				<img style="width: 140px; height:auto; padding:20px 0px 0px 0px;" src="ASSETS/IMAGE/Buyer hub.png">
			</div>
			<div class="col-md-12 text-center">
				<h2 class="modal-title" style="padding: 0px 0px 20px 0px;">Welcome Shopper!</h2>
			</div>
			<div class="col-md-12">';
				if (isset($_SESSION['failed'])){
					echo $_SESSION['failed'];
				}
				echo
				'
				<form autocomplete="off" action="FUNCTIONS/fLogin.php" method="post">
					<div class="form-group">
					<label style="font-size: 18px font-weight:35;"> <span class="fa fa-user-circle-o"></span> Username or E-mail *</label>
						<input type="text" class="form-control" name="txtUsername" id="txtUsername" placeholder="Enter your username" required>
					<label style="font-size: 18px; margin-top:1%; font-weight:35;"><span class="fa fa-lock"></span> Password *</label>
						<input type="password" class="form-control" name="txtPassword" id="txtPassword" placeholder="Enter your password" required>
					</div>
					<div class="text-center">
						<button class="btn btn-success" type="submit" style="margin-bottom:px;" name="login"><span class="fa fa-check-circle"></span> Log In</button>
					</div>
				</form>
				<div style="margin-top:1%" class="text-center">
					<a style="font-weight:100; text-decoration:none" href="forgotpassword">Forgot your password?</a>
				</div>
				<div style="margin:0.5% 0% 2% 0%" class="text-center">
					<a style="font-weight:100; text-decoration:none" href="registration">New user? Create your free account now.</a>
				</div>
			</div>
		</div>
	</div>
</div>
';